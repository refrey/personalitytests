package app.autoidlabs.ch.personalitytest.helper;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import app.autoidlabs.ch.personalitytest.R;

public class Likert6Fragment extends Fragment {

    View rootView;
    int layoutId;
    Activity activity;
    SeekBar seek;

    int currentValue = -1;
    boolean touched = false;

    String question;
    String questionId;

    public Likert6Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_likert6, null);
        activity = this.getActivity();

        question = this.getArguments().getString("question");
        questionId = this.getArguments().getString("id");

        TextView button1 = (TextView) rootView.findViewById(R.id.button1);
        button1.setText(this.getArguments().getString("button1"));
        TextView button2 = (TextView) rootView.findViewById(R.id.button2);
        button2.setText(this.getArguments().getString("button2"));
        TextView button3 = (TextView) rootView.findViewById(R.id.button3);
        button3.setText(this.getArguments().getString("button3"));
        TextView button4 = (TextView) rootView.findViewById(R.id.button4);
        button4.setText(this.getArguments().getString("button4"));
        TextView button5 = (TextView) rootView.findViewById(R.id.button5);
        button5.setText(this.getArguments().getString("button5"));
        TextView button6 = (TextView) rootView.findViewById(R.id.button6);
        button6.setText(this.getArguments().getString("button6"));

        rootView.setBackgroundColor(this.getArguments().getInt("color"));

        TextView v2 = (TextView) rootView.findViewById(R.id.question);
        v2.setText(question);

        info.hoang8f.android.segmented.SegmentedGroup segmentedGroup=(info.hoang8f.android.segmented.SegmentedGroup) rootView.findViewById(R.id.segment);

        segmentedGroup.setTintColor(Color.parseColor("#FF444444"), Color.parseColor("#FFFFFFFF"));;

        segmentedGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                touched = true;
                // Check which radio button was clicked
                switch(checkedId) {
                    case R.id.button1:
                        currentValue = 0;
                        break;
                    case R.id.button2:
                        currentValue = 1;
                        break;
                    case R.id.button3:
                        currentValue = 2;
                        break;
                    case R.id.button4:
                        currentValue = 3;
                        break;
                    case R.id.button5:
                        currentValue = 4;
                        break;
                    case R.id.button6:
                        currentValue = 5;
                        break;
                }

            }
        });

                    return rootView;
        }


    public String getQuestionId() {
        return questionId;
    }

    public int getAnswer() {
        if (touched) {
            return currentValue;
        }
        else {
            return -1;
        }
    }

    public boolean isAnswered() {
        return touched;
    }

}

