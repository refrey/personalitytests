package app.autoidlabs.ch.personalitytest.helper;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class HttpRequest {

    String url = "http://129.132.42.250/~frey/personality_2/";
    DefaultHttpClient client;

    public HttpRequest() {
        client = new DefaultHttpClient();
    }

    public ArrayList<String> getBig5AvgRating() throws JSONException {

        HttpGet request = new HttpGet(url + "get_big5_avg_rating.php");

        BufferedReader in = null;
        String data = null;
        JSONObject jArray = null;

        try{
            HttpResponse response = client.execute(request);
            Log.i("Response of GET request", response.toString());

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
            Log.i("data is",data);

            try {
                jArray = new JSONObject(data);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> avgRating = new ArrayList<String>();
        JSONArray raw_data = jArray.getJSONArray("basic");

        for (int i = 0; i < raw_data.length(); i++) {
            JSONObject r = raw_data.getJSONObject(i);
            if(i==0){
                String o = r.getString("O");
                avgRating.add(o);
            }else if(i==1){
                String c = r.getString("C");
                avgRating.add(c);
            }else if(i==2){
                String e = r.getString("E");
                avgRating.add(e);
            }else if(i==3){
                String a = r.getString("A");
                avgRating.add(a);
            }else if(i==4){
                String n = r.getString("N");
                avgRating.add(n);
            }
        }

        return avgRating;
    }

    public ArrayList<String> getLifeEventsAvgRating() throws JSONException {

        HttpGet request = new HttpGet(url + "get_life_events_avg_rating.php");

        BufferedReader in = null;
        String data = null;
        JSONObject jArray = null;

        try{
            HttpResponse response = client.execute(request);
            Log.i("Response of GET request", response.toString());

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
            Log.i("data is",data);

            try {
                jArray = new JSONObject(data);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> avgRating = new ArrayList<String>();
        JSONArray raw_data = jArray.getJSONArray("basic");

        for (int i = 0; i < raw_data.length(); i++) {
            JSONObject r = raw_data.getJSONObject(i);
            if(i==0){
                avgRating.add(r.getString("WE"));
            }else if(i==1){
                avgRating.add(r.getString("F"));
            }else if(i==2){
                avgRating.add(r.getString("HL"));
            }else if(i==3){
                avgRating.add(r.getString("HW"));
            }else if(i==4){
                avgRating.add(r.getString("TE"));
            }
        }

        return avgRating;
    }

    public ArrayList<String> getDarkTriadAvgRating() throws JSONException {

        HttpGet request = new HttpGet(url + "get_dark_triad_avg_rating.php");

        BufferedReader in = null;
        String data = null;
        JSONObject jArray = null;

        try{
            HttpResponse response = client.execute(request);
            Log.i("Response of GET request", response.toString());

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
            Log.i("data is",data);

            try {
                jArray = new JSONObject(data);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> avgRating = new ArrayList<String>();
        JSONArray raw_data = jArray.getJSONArray("basic");

        for (int i = 0; i < raw_data.length(); i++) {
            JSONObject r = raw_data.getJSONObject(i);
            if(i==0){
                avgRating.add(r.getString("M"));
            }else if(i==1){
                avgRating.add(r.getString("N"));
            }else if(i==2){
                avgRating.add(r.getString("P"));
            }
        }

        return avgRating;
    }

    public ArrayList<String> getInterestsAvgRating() throws JSONException {

        HttpGet request = new HttpGet(url + "get_interests_avg_rating.php");

        BufferedReader in = null;
        String data = null;
        JSONObject jArray = null;

        try{
            HttpResponse response = client.execute(request);
            Log.i("Response of GET request", response.toString());

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
            Log.i("data is",data);

            try {
                jArray = new JSONObject(data);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> avgRating = new ArrayList<String>();
        JSONArray raw_data = jArray.getJSONArray("basic");

        for (int i = 0; i < raw_data.length(); i++) {
            JSONObject r = raw_data.getJSONObject(i);
            if(i==0){
                avgRating.add(r.getString("R"));
            }else if(i==1){
                avgRating.add(r.getString("I"));
            }else if(i==2){
                avgRating.add(r.getString("A"));
            }else if(i==3){
                avgRating.add(r.getString("S"));
            }else if(i==4){
                avgRating.add(r.getString("E"));
            }else if(i==5){
                avgRating.add(r.getString("C"));
            }
        }

        return avgRating;
    }

    public ArrayList<String> getMyBig5Rating(String userId) throws JSONException {

        HttpGet request = new HttpGet(url + "get_my_big5_rating.php?id=" + userId);

        BufferedReader in = null;
        String data = null;
        JSONObject jArray = null;

        try{
            HttpResponse response = client.execute(request);
            Log.i("Response of GET request", response.toString());

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
            Log.i("data is",data);

            try {
                jArray = new JSONObject(data);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> myRating = new ArrayList<String>();
        JSONArray raw_data = jArray.getJSONArray("basic");

        for (int i = 0; i < raw_data.length(); i++) {
            JSONObject r = raw_data.getJSONObject(i);
            if(i==0){
                String o = r.getString("O");
                myRating.add(o);
            }else if(i==1){
                String c = r.getString("C");
                myRating.add(c);
            }else if(i==2){
                String e = r.getString("E");
                myRating.add(e);
            }else if(i==3){
                String a = r.getString("A");
                myRating.add(a);
            }else if(i==4){
                String n = r.getString("N");
                myRating.add(n);
            }
        }

        return myRating;
    }

    public ArrayList<String> getMyLifeEventsRating(String userId) throws JSONException {

        HttpGet request = new HttpGet(url + "get_my_life_events_rating.php?id=" + userId);

        BufferedReader in = null;
        String data = null;
        JSONObject jArray = null;

        try{
            HttpResponse response = client.execute(request);
            Log.i("Response of GET request", response.toString());

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
            Log.i("data is",data);

            try {
                jArray = new JSONObject(data);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> myRating = new ArrayList<String>();
        JSONArray raw_data = jArray.getJSONArray("basic");

        for (int i = 0; i < raw_data.length(); i++) {
            JSONObject r = raw_data.getJSONObject(i);
            if(i==0){
                myRating.add(r.getString("WE"));
            }else if(i==1){
                myRating.add(r.getString("F"));
            }else if(i==2){
                myRating.add(r.getString("HL"));
            }else if(i==3){
                myRating.add(r.getString("HW"));
            }else if(i==4){
                myRating.add(r.getString("TE"));
            }
        }

        return myRating;
    }

    public ArrayList<String> getMyDarkTriadRating(String userId) throws JSONException {

        HttpGet request = new HttpGet(url + "get_my_dark_triad_rating.php?id=" + userId);

        BufferedReader in = null;
        String data = null;
        JSONObject jArray = null;

        try{
            HttpResponse response = client.execute(request);
            Log.i("Response of GET request", response.toString());

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
            Log.i("data is",data);

            try {
                jArray = new JSONObject(data);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> myRating = new ArrayList<String>();
        JSONArray raw_data = jArray.getJSONArray("basic");

        for (int i = 0; i < raw_data.length(); i++) {
            JSONObject r = raw_data.getJSONObject(i);
            if(i==0){
                myRating.add(r.getString("M"));
            }else if(i==1){
                myRating.add(r.getString("N"));
            }else if(i==2){
                myRating.add(r.getString("P"));
            }
        }

        return myRating;
    }

    public ArrayList<String> getMyInterestsRating(String userId) throws JSONException {

        HttpGet request = new HttpGet(url + "get_my_interests_rating.php?id=" + userId);

        BufferedReader in = null;
        String data = null;
        JSONObject jArray = null;

        try{
            HttpResponse response = client.execute(request);
            Log.i("Response of GET request", response.toString());

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
            Log.i("data is",data);

            try {
                jArray = new JSONObject(data);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        ArrayList<String> myRating = new ArrayList<String>();
        JSONArray raw_data = jArray.getJSONArray("basic");

        for (int i = 0; i < raw_data.length(); i++) {
            JSONObject r = raw_data.getJSONObject(i);
            if(i==0){
                myRating.add(r.getString("R"));
            }else if(i==1){
                myRating.add(r.getString("I"));
            }else if(i==2){
                myRating.add(r.getString("A"));
            }else if(i==3){
                myRating.add(r.getString("S"));
            }else if(i==4){
                myRating.add(r.getString("E"));
            }else if(i==5){
                myRating.add(r.getString("C"));
            }
        }

        return myRating;
    }

    public String getWHO5YourScore(String userId) throws JSONException {

        HttpGet request = new HttpGet(url + "get_my_who5_score.php?id=" + userId);

        BufferedReader in = null;
        String data = null;
        JSONObject jArray = null;

        try{
            HttpResponse response = client.execute(request);
            Log.i("Response of GET request", response.toString());

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
            Log.i("data is",data);

            try {
                jArray = new JSONObject(data);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONArray raw_data = jArray.getJSONArray("basic");
        JSONObject r = raw_data.getJSONObject(0);
        String score = r.getString("S");
        return score;
    }

    public String getWHO5AvgScore() throws JSONException {

        HttpGet request = new HttpGet(url + "get_who5_avg_score.php");

        BufferedReader in = null;
        String data = null;
        JSONObject jArray = null;

        try{
            HttpResponse response = client.execute(request);
            Log.i("Response of GET request", response.toString());

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            StringBuffer sb = new StringBuffer("");
            String l = "";
            String nl = System.getProperty("line.separator");
            while ((l = in.readLine()) != null) {
                sb.append(l + nl);
            }
            in.close();
            data = sb.toString();
            Log.i("data is",data);

            try {
                jArray = new JSONObject(data);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        JSONArray raw_data = jArray.getJSONArray("basic");
        JSONObject r = raw_data.getJSONObject(0);
        String score = r.getString("S");
        return score;
    }

    public void saveBigFive(String userId, double vo, double vc, double ve, double va, double vn) {

        List<NameValuePair> data = new ArrayList<NameValuePair>(6);
        data.add(new BasicNameValuePair("id", userId));
        data.add(new BasicNameValuePair("o", String.valueOf(vo)));
        data.add(new BasicNameValuePair("c", String.valueOf(vc)));
        data.add(new BasicNameValuePair("e", String.valueOf(ve)));
        data.add(new BasicNameValuePair("a", String.valueOf(va)));
        data.add(new BasicNameValuePair("n", String.valueOf(vn)));

        //Log.i("========= in http", "data prepared");

        HttpPost httpPost = new HttpPost(url + "save_big5_estimation.php");
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(data));
            HttpResponse response = client.execute(httpPost);
            Log.i("Http Post Response:", response.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveLifeEvents(String userId, int we, int f, int hl, int hw, int te) {

        List<NameValuePair> data = new ArrayList<NameValuePair>(6);
        data.add(new BasicNameValuePair("id", userId));
        data.add(new BasicNameValuePair("we", String.valueOf(we)));
        data.add(new BasicNameValuePair("f", String.valueOf(f)));
        data.add(new BasicNameValuePair("hl", String.valueOf(hl)));
        data.add(new BasicNameValuePair("hw", String.valueOf(hw)));
        data.add(new BasicNameValuePair("te", String.valueOf(te)));

        HttpPost httpPost = new HttpPost(url + "save_life_events_estimation.php");
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(data));
            HttpResponse response = client.execute(httpPost);
            Log.i("Http Post Response:", response.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveDarkTriad(String userId, double vm, double vn, double vp) {

        List<NameValuePair> data = new ArrayList<NameValuePair>(4);
        data.add(new BasicNameValuePair("id", userId));
        data.add(new BasicNameValuePair("m", String.valueOf(vm)));
        data.add(new BasicNameValuePair("n", String.valueOf(vn)));
        data.add(new BasicNameValuePair("p", String.valueOf(vp)));

        HttpPost httpPost = new HttpPost(url + "save_dark_triad_estimation.php");
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(data));
            HttpResponse response = client.execute(httpPost);
            Log.i("Http Post Response:", response.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveInterests(String userId, double r, double i, double a, double s, double e, double c) {

        List<NameValuePair> data = new ArrayList<NameValuePair>(7);
        data.add(new BasicNameValuePair("id", userId));
        data.add(new BasicNameValuePair("r", String.valueOf(r)));
        data.add(new BasicNameValuePair("i", String.valueOf(i)));
        data.add(new BasicNameValuePair("a", String.valueOf(a)));
        data.add(new BasicNameValuePair("s", String.valueOf(s)));
        data.add(new BasicNameValuePair("e", String.valueOf(e)));
        data.add(new BasicNameValuePair("c", String.valueOf(c)));

        HttpPost httpPost = new HttpPost(url + "save_interests_estimation.php");
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(data));
            HttpResponse response = client.execute(httpPost);
            Log.i("Http Post Response:", response.toString());
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveAnswers(String userId, List<NameValuePair> data) {

        data.add(new BasicNameValuePair("id", userId));

        HttpPost httpPost = new HttpPost(url + "save_answers.php");
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(data));
            HttpResponse response = client.execute(httpPost);
            Log.d("Http Post Response:", response.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveUserWithAdId(String userId, Context context) {
        List<NameValuePair> data = new ArrayList<NameValuePair>(9);
        data.add(new BasicNameValuePair("id", userId));

        String AdId;
        try {
            AdId = AdvertisingIdClient.getAdvertisingIdInfo(context).getId();
        } catch (GooglePlayServicesRepairableException e) {
            AdId = "GooglePlayServicesRepairableException";
        } catch (GooglePlayServicesNotAvailableException e) {
            AdId = "GooglePlayServicesNotAvailableException";
        } catch (IOException e) {
            AdId = "IOException";
        }
        data.add(new BasicNameValuePair("adid", AdId));

        String language = Locale.getDefault().toString();
        data.add(new BasicNameValuePair("language", language));

        HttpPost httpPost = new HttpPost(url + "save_user_with_adid.php");
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(data));
            HttpResponse response = client.execute(httpPost);
            Log.d("Http Post Response:", response.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveApp(String userId, String appName, String packageName, int firstInstallTime, int lastUpdateTime, String versionName, int isPreInstalled, int isRunning) {
        List<NameValuePair> data = new ArrayList<NameValuePair>(9);
        data.add(new BasicNameValuePair("id", userId));
        data.add(new BasicNameValuePair("app", appName));
        data.add(new BasicNameValuePair("package", packageName));
        data.add(new BasicNameValuePair("install_time", String.valueOf(firstInstallTime)));
        data.add(new BasicNameValuePair("update_time", String.valueOf(lastUpdateTime)));
        data.add(new BasicNameValuePair("version", versionName));
        data.add(new BasicNameValuePair("pre_installed", String.valueOf(isPreInstalled)));
        data.add(new BasicNameValuePair("running", String.valueOf(isRunning)));

        HttpPost httpPost = new HttpPost(url + "save_app.php");
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(data));
            HttpResponse response = client.execute(httpPost);
            Log.d("Http Post Response:", response.toString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}