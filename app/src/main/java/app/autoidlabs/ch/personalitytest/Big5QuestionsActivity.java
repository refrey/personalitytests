package app.autoidlabs.ch.personalitytest;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.autoidlabs.ch.personalitytest.helper.HttpRequest;
import app.autoidlabs.ch.personalitytest.helper.Likert5Fragment;

public class Big5QuestionsActivity extends Activity {

    public String userId;
    List<Likert5Fragment> fragmentList = null;

    ArrayList<String> myRating = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the userId from the intent
        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");

        setContentView(R.layout.activity_likert_page);

        getActionBar().hide();

        // set title
        TextView titleView = (TextView) findViewById(R.id.title_activity_likert_page_text);
        titleView.setText(R.string.title_activity_big5);
        // set icon
        ImageView imageView = (ImageView) findViewById(R.id.likert_page_icon);
        imageView.setImageResource(R.drawable.battery2);
        // set color
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.logo_container);
        linearLayout.setBackgroundColor(0xAAFFA400);
        // set footer
        TextView footer = (TextView) findViewById(R.id.footer_text);
        footer.setText(R.string.page11);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        List<NameValuePair> questions = new ArrayList<NameValuePair>();
        questions.add(new BasicNameValuePair("301", getString(R.string.big5_1)));
        questions.add(new BasicNameValuePair("302", getString(R.string.big5_2)));
        questions.add(new BasicNameValuePair("303", getString(R.string.big5_3)));
        questions.add(new BasicNameValuePair("304", getString(R.string.big5_4)));
        questions.add(new BasicNameValuePair("305", getString(R.string.big5_5)));
        questions.add(new BasicNameValuePair("306", getString(R.string.big5_6)));
        questions.add(new BasicNameValuePair("307", getString(R.string.big5_7)));
        questions.add(new BasicNameValuePair("308", getString(R.string.big5_8)));
        questions.add(new BasicNameValuePair("309", getString(R.string.big5_9)));
        questions.add(new BasicNameValuePair("310", getString(R.string.big5_10)));
        questions.add(new BasicNameValuePair("311", getString(R.string.big5_11)));
        questions.add(new BasicNameValuePair("312", getString(R.string.big5_12)));
        questions.add(new BasicNameValuePair("313", getString(R.string.big5_13)));
        questions.add(new BasicNameValuePair("314", getString(R.string.big5_14)));
        questions.add(new BasicNameValuePair("315", getString(R.string.big5_15)));
        questions.add(new BasicNameValuePair("316", getString(R.string.big5_16)));
        questions.add(new BasicNameValuePair("317", getString(R.string.big5_17)));
        questions.add(new BasicNameValuePair("318", getString(R.string.big5_18)));
        questions.add(new BasicNameValuePair("319", getString(R.string.big5_19)));
        questions.add(new BasicNameValuePair("320", getString(R.string.big5_20)));
        questions.add(new BasicNameValuePair("321", getString(R.string.big5_21)));
        questions.add(new BasicNameValuePair("322", getString(R.string.big5_22)));
        questions.add(new BasicNameValuePair("323", getString(R.string.big5_23)));
        questions.add(new BasicNameValuePair("324", getString(R.string.big5_24)));
        questions.add(new BasicNameValuePair("325", getString(R.string.big5_25)));
        questions.add(new BasicNameValuePair("326", getString(R.string.big5_26)));
        questions.add(new BasicNameValuePair("327", getString(R.string.big5_27)));
        questions.add(new BasicNameValuePair("328", getString(R.string.big5_28)));
        questions.add(new BasicNameValuePair("330", getString(R.string.big5_30)));
        questions.add(new BasicNameValuePair("331", getString(R.string.big5_31)));
        questions.add(new BasicNameValuePair("332", getString(R.string.big5_32)));
        questions.add(new BasicNameValuePair("333", getString(R.string.big5_33)));
        questions.add(new BasicNameValuePair("334", getString(R.string.big5_34)));
        questions.add(new BasicNameValuePair("335", getString(R.string.big5_35)));
        questions.add(new BasicNameValuePair("336", getString(R.string.big5_36)));
        questions.add(new BasicNameValuePair("337", getString(R.string.big5_37)));
        questions.add(new BasicNameValuePair("338", getString(R.string.big5_38)));
        questions.add(new BasicNameValuePair("339", getString(R.string.big5_39)));
        questions.add(new BasicNameValuePair("340", getString(R.string.big5_40)));
        questions.add(new BasicNameValuePair("341", getString(R.string.big5_41)));
        questions.add(new BasicNameValuePair("343", getString(R.string.big5_43)));
        questions.add(new BasicNameValuePair("344", getString(R.string.big5_44)));


        int color1 = 0x00FFFFFF;
        int color2 = 0xBBFFFFFF;
        boolean isColor1 = true;

        fragmentList = new ArrayList<Likert5Fragment>();

        // display random order
        //Collections.shuffle(questions);

        for (NameValuePair question : questions) {
            Likert5Fragment fragment = new Likert5Fragment();
            fragmentList.add(fragment);
            Bundle args = new Bundle();
            args.putString("question", (String) question.getValue());
            args.putString("id", question.getName());
            args.putInt("mode", 2);
            if (isColor1) {
                args.putInt("color", color1);
            } else {
                args.putInt("color", color2);
            }
            isColor1 = !isColor1;

            args.putString("button1", getString(R.string.likert_agree_1));
            args.putString("button2", getString(R.string.likert_agree_2));
            args.putString("button3", getString(R.string.likert_agree_3));
            args.putString("button4", getString(R.string.likert_agree_4));
            args.putString("button5", getString(R.string.likert_agree_5));

            fragment.setArguments(args);
            fragmentTransaction.add(R.id.container, fragment);
        }
        fragmentTransaction.commit();

    }

    public void nextPage(View view) {
        // check if all questions are answered
        boolean allAnswered = true;
        for (Likert5Fragment fragment : fragmentList) {
            allAnswered = allAnswered && fragment.isAnswered();
        }
        if (allAnswered) { // go to next page
            computeMyRating();
            new Thread(new SaveAnswersTask()).start();
            new Thread(new SaveBigFiveTask()).start();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String state = prefs.getString("demographics_state", "");
            Intent intent;
            if (state.equals("finished")) {
                intent = new Intent(this, Big5ResultsActivity.class);
            }
            else {
                intent = new Intent(this, DemographicsActivity.class);
                intent.putExtra("nextActivity", "big5");
            }
            intent.putExtra("userId", userId);
            startActivity(intent);

        } else { // display error message
            this.findViewById(R.id.error).setVisibility(View.VISIBLE);
        }

    }

    class SaveAnswersTask implements Runnable {
        @Override
        public void run() {
            List<NameValuePair> answers = new ArrayList<NameValuePair>(fragmentList.size());
            for (Likert5Fragment fragment : fragmentList) {
                String questionId = fragment.getQuestionId();
                int answer = fragment.getAnswer();
                answers.add(new BasicNameValuePair(questionId, Integer.toString(answer)));
            }
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveAnswers(userId, answers);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("big5_state", "finished");
            editor.commit();
        }
    }

    private void computeMyRating() {
        ArrayList<Integer> o_plus = new ArrayList<Integer>();
        ArrayList<Integer> o_minus = new ArrayList<Integer>();
        ArrayList<Integer> c_plus = new ArrayList<Integer>();
        ArrayList<Integer> c_minus = new ArrayList<Integer>();
        ArrayList<Integer> e_plus = new ArrayList<Integer>();
        ArrayList<Integer> e_minus = new ArrayList<Integer>();
        ArrayList<Integer> a_plus = new ArrayList<Integer>();
        ArrayList<Integer> a_minus = new ArrayList<Integer>();
        ArrayList<Integer> n_plus = new ArrayList<Integer>();
        ArrayList<Integer> n_minus = new ArrayList<Integer>();

        for (Likert5Fragment fragment : fragmentList) {
            String questionId = fragment.getQuestionId();
            int answer = fragment.getAnswer();
            String[] qn_e_plus = new String[] {"301","311","316","326","336"};
            String[] qn_e_minus = new String[]{"306","321","331"};
            String[] qn_o_plus = new String[] {"305","310","315","320","325","330","340","344"};
            String[] qn_o_minus = new String[]{"335","341"};
            String[] qn_c_plus = new String[] {"303","313","328","333","338"};
            String[] qn_c_minus = new String[] {"308","318","323","343"};
            String[] qn_a_plus = new String[] {"307","317","322","332"};
            String[] qn_a_minus = new String[] {"302","312","327","337"};
            String[] qn_n_plus = new String[] {"304","314","319","339"};
            String[] qn_n_minus = new String[] {"309","324","334"};

            if(Arrays.asList(qn_e_plus).contains(questionId)){
                e_plus.add(answer);
            }else if(Arrays.asList(qn_e_minus).contains(questionId)){
                e_minus.add(answer);
            }else if(Arrays.asList(qn_o_plus).contains(questionId)){
                o_plus.add(answer);
            }else if(Arrays.asList(qn_o_minus).contains(questionId)){
                o_minus.add(answer);
            }else if(Arrays.asList(qn_c_plus).contains(questionId)){
                c_plus.add(answer);
            }else if(Arrays.asList(qn_c_minus).contains(questionId)){
                c_minus.add(answer);
            }else if(Arrays.asList(qn_a_plus).contains(questionId)){
                a_plus.add(answer);
            }else if(Arrays.asList(qn_a_minus).contains(questionId)) {
                a_minus.add(answer);
            }else if(Arrays.asList(qn_n_plus).contains(questionId)){
                n_plus.add(answer);
            }else if(Arrays.asList(qn_n_minus).contains(questionId)){
                n_minus.add(answer);
            }
        }

        double vo = (6*2.0 + sumList(o_plus) - sumList(o_minus))/10.0;
        double vc = (6*4.0 + sumList(c_plus) - sumList(c_minus))/9.0;
        double ve = (6*3.0 + sumList(e_plus) - sumList(e_minus))/8.0;
        double va = (6*4.0 + sumList(a_plus) - sumList(a_minus))/8.0;
        double vn = (6*3.0 + sumList(n_plus) - sumList(n_minus))/7.0;

        String s_vo = String.valueOf(vo);
        String s_vc = String.valueOf(vc);
        String s_ve = String.valueOf(ve);
        String s_va = String.valueOf(va);
        String s_vn = String.valueOf(vn);

        Log.i("vo = ", s_vo);
        Log.i("vc = ", s_vc);
        Log.i("ve = ", s_ve);
        Log.i("va = ", s_va);
        Log.i("vn = ", s_vn);

        myRating = new ArrayList<String>();
        myRating.add(s_vo);
        myRating.add(s_vc);
        myRating.add(s_ve);
        myRating.add(s_va);
        myRating.add(s_vn);
    }

    private class SaveBigFiveTask implements Runnable {
        @Override
        public void run() {
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveBigFive(userId, Double.parseDouble(myRating.get(0)), Double.parseDouble(myRating.get(1)),
                    Double.parseDouble(myRating.get(2)), Double.parseDouble(myRating.get(3)),Double.parseDouble(myRating.get(4)));
        }
    }


    public double sumList (ArrayList<Integer> obj){
        double sum = 0.0;
        for(int i = 0; i < obj.size(); i++)
        {
            sum = sum + obj.get(i);
        }
        return sum;
    }
}
