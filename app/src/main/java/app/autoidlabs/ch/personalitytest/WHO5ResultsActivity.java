package app.autoidlabs.ch.personalitytest;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.RadarChart;

import org.json.JSONException;

import java.util.ArrayList;

import app.autoidlabs.ch.personalitytest.helper.HttpRequest;

public class WHO5ResultsActivity extends FragmentActivity {

    private static RadarChart mChart;
    static Context context;
    private ProgressDialog dialog;
    private ByeFragment fragment;

    static ArrayList<String> avgRating = null;
    static ArrayList<String> myRating = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();

        setContentView(R.layout.activity_main);

        getActionBar().hide();

        fragment = new ByeFragment();
        getFragmentManager().beginTransaction().add(R.id.container, fragment).commit();

        new Thread(new getRatingsTask()).start();
        BroadcastReceiver resultReceiver = createBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(resultReceiver, new IntentFilter("who5_rating"));
    }

    private BroadcastReceiver createBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String yourScore = intent.getStringExtra("yourScore");
                String avgScore = intent.getStringExtra("avgScore");

                TextView yourScoreView = (TextView) findViewById(R.id.who5_your_score);
                yourScoreView.setText(context.getString(R.string.your_rating) + ": " + yourScore);
                TextView avgScoreView = (TextView) findViewById(R.id.who5_avg_score);
                avgScoreView.setText(context.getString(R.string.average_rating) + ": " + avgScore);
            }
        };
    }

    private class getRatingsTask implements Runnable {
        public void run() {
            HttpRequest httpRequest = new HttpRequest();
            String yourScore = null;
            String avgScore = null;

            // get the userId from the intent
            String userId = getIntent().getStringExtra("userId");

            try {
                yourScore = httpRequest.getWHO5YourScore(userId);
                avgScore = httpRequest.getWHO5AvgScore();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // broadcast asynchronously
            Intent intent = new Intent("who5_rating");
            intent.putExtra("yourScore", yourScore);
            intent.putExtra("avgScore", avgScore);
            LocalBroadcastManager.getInstance(WHO5ResultsActivity.this).sendBroadcast(intent);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    static public class ByeFragment extends Fragment {

        View rootView;
        Activity activity;

        public ByeFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_who5_results, container, false);
            activity = this.getActivity();

            TextView yourScoreView = (TextView) rootView.findViewById(R.id.who5_your_score);
            yourScoreView.setText(context.getString(R.string.your_rating) + ": " + context.getString(R.string.loading));
            TextView avgScoreView = (TextView) rootView.findViewById(R.id.who5_avg_score);
            avgScoreView.setText(context.getString(R.string.average_rating) + ": " + context.getString(R.string.loading));

            return rootView;

        }

    }


}

