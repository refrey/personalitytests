package app.autoidlabs.ch.personalitytest;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CheckBox;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import app.autoidlabs.ch.personalitytest.helper.HttpRequest;

public class InterestsQuestionsActivity extends Activity {

    public String userId;
    ArrayList<Integer> myRating = null;
    List<NameValuePair> answers = new ArrayList<NameValuePair>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the userId from the intent
        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");

        setContentView(R.layout.activity_interests);

        getActionBar().hide();

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
    }

    public void nextPage(View view) {
            prepareAnswers();
            new Thread(new SaveAnswersTask()).start();

            Intent intent = new Intent(this, InterestsRIASECQuestionsActivity.class);
            intent.putExtra("userId", userId);
            startActivity(intent);
    }

    class SaveAnswersTask implements Runnable {
        @Override
        public void run() {
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveAnswers(userId, answers);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("interests_state", "finished");
            editor.commit();
        }
    }

    private void prepareAnswers() {

        if (((CheckBox) findViewById(R.id.checkbox_interest_1)).isChecked()) { answers.add(new BasicNameValuePair("8001", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_2)).isChecked()) { answers.add(new BasicNameValuePair("8002", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_3)).isChecked()) { answers.add(new BasicNameValuePair("8003", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_4)).isChecked()) { answers.add(new BasicNameValuePair("8004", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_5)).isChecked()) { answers.add(new BasicNameValuePair("8005", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_6)).isChecked()) { answers.add(new BasicNameValuePair("8006", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_7)).isChecked()) { answers.add(new BasicNameValuePair("8007", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_8)).isChecked()) { answers.add(new BasicNameValuePair("8008", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_9)).isChecked()) { answers.add(new BasicNameValuePair("8009", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_10)).isChecked()) { answers.add(new BasicNameValuePair("8010", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_11)).isChecked()) { answers.add(new BasicNameValuePair("8011", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_12)).isChecked()) { answers.add(new BasicNameValuePair("8012", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_13)).isChecked()) { answers.add(new BasicNameValuePair("8013", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_14)).isChecked()) { answers.add(new BasicNameValuePair("8014", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_15)).isChecked()) { answers.add(new BasicNameValuePair("8015", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_16)).isChecked()) { answers.add(new BasicNameValuePair("8016", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_17)).isChecked()) { answers.add(new BasicNameValuePair("8017", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_18)).isChecked()) { answers.add(new BasicNameValuePair("8018", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_19)).isChecked()) { answers.add(new BasicNameValuePair("8019", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_20)).isChecked()) { answers.add(new BasicNameValuePair("8020", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_21)).isChecked()) { answers.add(new BasicNameValuePair("8021", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_22)).isChecked()) { answers.add(new BasicNameValuePair("8022", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_23)).isChecked()) { answers.add(new BasicNameValuePair("8023", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_24)).isChecked()) { answers.add(new BasicNameValuePair("8024", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_25)).isChecked()) { answers.add(new BasicNameValuePair("8025", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_26)).isChecked()) { answers.add(new BasicNameValuePair("8026", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_27)).isChecked()) { answers.add(new BasicNameValuePair("8027", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_28)).isChecked()) { answers.add(new BasicNameValuePair("8028", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_29)).isChecked()) { answers.add(new BasicNameValuePair("8029", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_30)).isChecked()) { answers.add(new BasicNameValuePair("8030", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_31)).isChecked()) { answers.add(new BasicNameValuePair("8031", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_32)).isChecked()) { answers.add(new BasicNameValuePair("8032", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_33)).isChecked()) { answers.add(new BasicNameValuePair("8033", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_34)).isChecked()) { answers.add(new BasicNameValuePair("8034", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_35)).isChecked()) { answers.add(new BasicNameValuePair("8035", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_36)).isChecked()) { answers.add(new BasicNameValuePair("8036", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_37)).isChecked()) { answers.add(new BasicNameValuePair("8037", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_38)).isChecked()) { answers.add(new BasicNameValuePair("8038", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_39)).isChecked()) { answers.add(new BasicNameValuePair("8039", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_40)).isChecked()) { answers.add(new BasicNameValuePair("8040", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_41)).isChecked()) { answers.add(new BasicNameValuePair("8041", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_42)).isChecked()) { answers.add(new BasicNameValuePair("8042", "1")); }
        if (((CheckBox) findViewById(R.id.checkbox_interest_43)).isChecked()) { answers.add(new BasicNameValuePair("8043", "1")); }

    }

}

