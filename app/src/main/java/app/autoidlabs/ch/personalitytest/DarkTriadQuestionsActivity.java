package app.autoidlabs.ch.personalitytest;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import app.autoidlabs.ch.personalitytest.helper.HttpRequest;
import app.autoidlabs.ch.personalitytest.helper.Likert5Fragment;

public class DarkTriadQuestionsActivity extends Activity {

    public String userId;
    List<Likert5Fragment> fragmentList = null;

    ArrayList<String> myRating = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the userId from the intent
        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");

        setContentView(R.layout.activity_likert_page);

        getActionBar().hide();

        // set title
        TextView titleView = (TextView) findViewById(R.id.title_activity_likert_page_text);
        titleView.setText(R.string.title_activity_dark_triad);
        // set icon
        ImageView imageView = (ImageView) findViewById(R.id.likert_page_icon);
        imageView.setImageResource(R.drawable.battery1);
        // set color
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.logo_container);
        linearLayout.setBackgroundColor(0xAAFF0000);
        // set footer
        TextView footer = (TextView) findViewById(R.id.footer_text);
        footer.setText(R.string.page12);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        List<NameValuePair> questions = new ArrayList<NameValuePair>();
        questions.add(new BasicNameValuePair("501", getString(R.string.dark_triad_1)));
        questions.add(new BasicNameValuePair("502", getString(R.string.dark_triad_2)));
        questions.add(new BasicNameValuePair("503", getString(R.string.dark_triad_3)));
        questions.add(new BasicNameValuePair("504", getString(R.string.dark_triad_4)));
        questions.add(new BasicNameValuePair("505", getString(R.string.dark_triad_5)));
        questions.add(new BasicNameValuePair("506", getString(R.string.dark_triad_6)));
        questions.add(new BasicNameValuePair("507", getString(R.string.dark_triad_7)));
        questions.add(new BasicNameValuePair("508", getString(R.string.dark_triad_8)));
        questions.add(new BasicNameValuePair("509", getString(R.string.dark_triad_9)));
        questions.add(new BasicNameValuePair("510", getString(R.string.dark_triad_10)));
        questions.add(new BasicNameValuePair("511", getString(R.string.dark_triad_11)));
        questions.add(new BasicNameValuePair("512", getString(R.string.dark_triad_12)));
        questions.add(new BasicNameValuePair("513", getString(R.string.dark_triad_13)));
        questions.add(new BasicNameValuePair("514", getString(R.string.dark_triad_14)));
        questions.add(new BasicNameValuePair("515", getString(R.string.dark_triad_15)));
        questions.add(new BasicNameValuePair("516", getString(R.string.dark_triad_16)));
        questions.add(new BasicNameValuePair("517", getString(R.string.dark_triad_17)));
        questions.add(new BasicNameValuePair("518", getString(R.string.dark_triad_18)));
        questions.add(new BasicNameValuePair("519", getString(R.string.dark_triad_19)));
        questions.add(new BasicNameValuePair("520", getString(R.string.dark_triad_20)));
        questions.add(new BasicNameValuePair("521", getString(R.string.dark_triad_21)));
        questions.add(new BasicNameValuePair("522", getString(R.string.dark_triad_22)));
        questions.add(new BasicNameValuePair("523", getString(R.string.dark_triad_23)));
        questions.add(new BasicNameValuePair("524", getString(R.string.dark_triad_24)));
        questions.add(new BasicNameValuePair("525", getString(R.string.dark_triad_25)));
        questions.add(new BasicNameValuePair("526", getString(R.string.dark_triad_26)));
        questions.add(new BasicNameValuePair("527", getString(R.string.dark_triad_27)));

        int color1 = 0x00FFFFFF;
        int color2 = 0xBBFFFFFF;
        boolean isColor1 = true;

        fragmentList = new ArrayList<Likert5Fragment>();

        // display random order
        //Collections.shuffle(questions);

        for (NameValuePair question : questions) {
            Likert5Fragment fragment = new Likert5Fragment();
            fragmentList.add(fragment);
            Bundle args = new Bundle();
            args.putString("question", (String) question.getValue());
            args.putString("id", question.getName());
            if (isColor1) {
                args.putInt("color", color1);
            } else {
                args.putInt("color", color2);
            }
            isColor1 = !isColor1;

            args.putString("button1", getString(R.string.likert_agree_1));
            args.putString("button2", getString(R.string.likert_agree_2));
            args.putString("button3", getString(R.string.likert_agree_3));
            args.putString("button4", getString(R.string.likert_agree_4));
            args.putString("button5", getString(R.string.likert_agree_5));

            fragment.setArguments(args);
            fragmentTransaction.add(R.id.container, fragment);
        }
        fragmentTransaction.commit();

    }

    public void nextPage(View view) {
        // check if all questions are answered
        boolean allAnswered = true;
        for (Likert5Fragment fragment : fragmentList) {
            allAnswered = allAnswered && fragment.isAnswered();
        }
        if (allAnswered) { // go to next page
            computeMyRating();
            new Thread(new SaveAnswersTask()).start();
            new Thread(new SaveDarkTriadTask()).start();

            Intent intent = new Intent(this, FinanceRiskQuestionsActivity.class);
            intent.putExtra("userId", userId);
            startActivity(intent);

        } else { // display error message
            this.findViewById(R.id.error).setVisibility(View.VISIBLE);
        }

    }

    class SaveAnswersTask implements Runnable {
        @Override
        public void run() {
            List<NameValuePair> answers = new ArrayList<NameValuePair>(fragmentList.size());
            for (Likert5Fragment fragment : fragmentList) {
                String questionId = fragment.getQuestionId();
                int answer = fragment.getAnswer();
                answers.add(new BasicNameValuePair(questionId, Integer.toString(answer)));
            }
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveAnswers(userId, answers);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("dark_triad_state", "finished"); // The risk finance questions are part of the Dark Triad questionnaire.
            editor.commit();
        }
    }

    private void computeMyRating() {
        ArrayList<Integer> m_plus = new ArrayList<Integer>();
        ArrayList<Integer> m_minus = new ArrayList<Integer>();
        ArrayList<Integer> n_plus = new ArrayList<Integer>();
        ArrayList<Integer> n_minus = new ArrayList<Integer>();
        ArrayList<Integer> p_plus = new ArrayList<Integer>();
        ArrayList<Integer> p_minus = new ArrayList<Integer>();

        for (Likert5Fragment fragment : fragmentList) {
            String questionId = fragment.getQuestionId();
            int answer = fragment.getAnswer();
            String[] qn_m_plus = new String[] {"501","504","507","510","513","516","519","522","525"};
            String[] qn_n_plus = new String[] {"502","508","511","514","520","526"};
            String[] qn_n_minus = new String[]{"505","517","523"};
            String[] qn_p_plus = new String[] {"503","509","512","515","518","524","527"};
            String[] qn_p_minus = new String[] {"506","521"};

            if(Arrays.asList(qn_m_plus).contains(questionId)){
                m_plus.add(answer);
            }else if(Arrays.asList(qn_n_plus).contains(questionId)){
                n_plus.add(answer);
            }else if(Arrays.asList(qn_n_minus).contains(questionId)){
                n_minus.add(answer);
            }else if(Arrays.asList(qn_p_plus).contains(questionId)){
                p_plus.add(answer);
            }else if(Arrays.asList(qn_p_minus).contains(questionId)) {
                p_minus.add(answer);
            }
        }

        double vm = (sumList(m_plus))/9.0;
        double vn = (6*3.0 + sumList(n_plus) - sumList(n_minus))/9.0;
        double vp = (6*2.0 + sumList(p_plus) - sumList(p_minus))/9.0;

        String s_vm = String.valueOf(vm);
        String s_vn = String.valueOf(vn);
        String s_vp = String.valueOf(vp);

        myRating = new ArrayList<String>();
        myRating.add(s_vm);
        myRating.add(s_vn);
        myRating.add(s_vp);
    }

    private class SaveDarkTriadTask implements Runnable {
        @Override
        public void run() {
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveDarkTriad(userId, Double.parseDouble(myRating.get(0)), Double.parseDouble(myRating.get(1)),
                    Double.parseDouble(myRating.get(2)));
        }
    }

    public double sumList (ArrayList<Integer> obj){
        double sum = 0.0;
        for(int i = 0; i < obj.size(); i++)
        {
            sum = sum + obj.get(i);
        }
        return sum;
    }
}
