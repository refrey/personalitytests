package app.autoidlabs.ch.personalitytest.helper;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class MySeekBar {

    private SeekBar seekBar;
    private TextView textView;
    private String questionText;
    private String answerText;
    private String answerText2;
    private LinearLayout fullSeekBar;

    private final float start=0;
    private final float end=12;
    private final double startValue = 0;
    private int currentValue = -1;
    private boolean touched = false;
    private boolean checked = false;

    public MySeekBar (LinearLayout ll, SeekBar sb, TextView tv, String qt, String at) {
        fullSeekBar = ll;
        seekBar = sb;
        textView = tv;
        questionText = qt;
        answerText = at;

        textView.setText(qt);

        int start_position = (int) Math.round(100/(end-start)*(startValue-start));
        seekBar.setProgress(start_position);
        seekBar.getThumb().setColorFilter(Color.LTGRAY, PorterDuff.Mode.SRC_IN);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                // To convert it as discrete value
                float temp = progress;
                float dis = end - start;
                currentValue = Math.round((start + ((temp / 100) * dis)));
                progress = (int) ((currentValue - start) / dis * 100);
                seekBar.setProgress(progress);
                String currentText = answerText.replace("XY", Integer.toString(currentValue));

                textView.setText(currentText);

                seekBar.getThumb().setColorFilter(Color.BLACK, PorterDuff.Mode.SRC_IN);
                touched = true;
            }
        });
    }

    public int getValue() {
        return currentValue;
    }

    public String getStringValue() {
        return Integer.toString(currentValue);
    }

    public boolean isChecked() {
        return checked;
    }

    public boolean isTouched() {
        return touched;
    }

    public boolean isReady() {
        return (touched && checked) || !checked;
    }

    public void onCheckBoxClick(boolean checked) {
        this.checked = checked;
        if (checked) {
            fullSeekBar.setVisibility(View.VISIBLE);
        }
        else {
            fullSeekBar.setVisibility(View.GONE);
        }
    }

    public void setRed() {
        textView.setTextColor(Color.RED);
    }

    public void setBlack() {
        textView.setTextColor(Color.BLACK);
    }

}
