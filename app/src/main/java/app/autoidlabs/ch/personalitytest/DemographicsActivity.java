package app.autoidlabs.ch.personalitytest;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import app.autoidlabs.ch.personalitytest.helper.HttpRequest;

public class DemographicsActivity extends Activity {

    public String userId;
    List<NameValuePair> answers;
    Class nextActivity = null;

    public String genderAnswer = "-1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the userId from the intent
        userId = getIntent().getStringExtra("userId");

        setContentView(R.layout.activity_demographics);

        getActionBar().hide();

        String nextActivityString = getIntent().getStringExtra("nextActivity");
        if (nextActivityString.equals("big5")) {
            nextActivity = Big5ResultsActivity.class;
        } else if (nextActivityString.equals("life_events")) {
            nextActivity = LifeEventsResultsActivity.class;
        } else if (nextActivityString.equals("dark_triad")) {
            nextActivity = DarkTriadResultsActivity.class;
        } else if (nextActivityString.equals("interests")) {
            nextActivity = InterestsResultsActivity.class;
        } else if (nextActivityString.equals("who5")) {
            nextActivity = WHO5ResultsActivity.class;
        }

    }

    public void onClickGender(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.radio_gender_female:
                if (checked)
                    genderAnswer = "female";
                break;
            case R.id.radio_gender_male:
                if (checked)
                    genderAnswer = "male";
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void nextPage(View view) {
        buildAnswerList();
        // check if all questions are answered
        boolean allAnswered = true;
        for (NameValuePair answer : answers) {
            String value = answer.getValue();
            allAnswered = allAnswered && !value.contains(getString(R.string.choose)) && !value.contains("-1") && !value.matches("");
        }
        if (allAnswered) { // go to next page
            new Thread(new SaveAnswersTask()).start();
            Intent newIntent = new Intent(this, nextActivity);
            newIntent.putExtra("userId", userId);
            startActivity(newIntent);
        } else { // display error message
            this.findViewById(R.id.error).setVisibility(View.VISIBLE);
        }
    }

    private void buildAnswerList() {
        answers = new ArrayList<NameValuePair>();
        answers.add(new BasicNameValuePair("601", genderAnswer));
        answers.add(getAnswerFromSpinner("603", R.id.spinner_partnership));
        answers.add(getAnswerFromSpinner("604", R.id.spinner_living));
        answers.add(getAnswerFromSpinner("605", R.id.spinner_children_number));
        answers.add(getAnswerFromSpinner("606", R.id.spinner_children_age));
        answers.add(getAnswerFromSpinner("607", R.id.spinner_job));
        answers.add(getAnswerFromSpinner("608", R.id.spinner_education));
        answers.add(getAnswerFromSpinner("609", R.id.spinner_income));
        answers.add(getAnswerFromEditText("610", R.id.edit_text_age));
        answers.add(getAnswerFromSpinner("614", R.id.spinner_politics)); // new since Dec 2017
        answers.add(getAnswerFromEditText("615", R.id.edit_text_party)); // new since Dec 2017
        answers.add(getAnswerFromEditText("611", R.id.edit_text_attention_test));
        //answers.add(getAnswerFromSpinner("612", R.id.spinner_commuting_type)); // removed since Dec 2017
        //answers.add(getAnswerFromSpinner("613", R.id.spinner_commuting_duration)); // removed since Dec 2017
    }

    private BasicNameValuePair getAnswerFromSpinner(String questionId, int viewId) {
        Spinner spinner = (Spinner) findViewById(viewId);
        String answer = spinner.getSelectedItem().toString();
        return new BasicNameValuePair(questionId, answer);
    }

    private BasicNameValuePair getAnswerFromEditText(String questionId, int viewId) {
        EditText editText = (EditText) findViewById(viewId);
        String answer = editText.getText().toString();
        return new BasicNameValuePair(questionId, answer);
    }


    class SaveAnswersTask implements Runnable {
        @Override
        public void run() {
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveAnswers(userId, answers);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("demographics_state", "finished");
            editor.commit();
        }
    }

}