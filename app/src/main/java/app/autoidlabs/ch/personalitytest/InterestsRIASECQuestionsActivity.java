package app.autoidlabs.ch.personalitytest;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.autoidlabs.ch.personalitytest.helper.HttpRequest;
import app.autoidlabs.ch.personalitytest.helper.Likert5Fragment;

public class InterestsRIASECQuestionsActivity extends Activity {

    public String userId;
    List<Likert5Fragment> fragmentList = null;

    ArrayList<Double> myRating = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the userId from the intent
        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");

        setContentView(R.layout.activity_likert_page);

        getActionBar().hide();

        // set title
        TextView titleView = (TextView) findViewById(R.id.title_activity_likert_page_text);
        titleView.setText(R.string.title_activity_interests_riasec);
        // set icon
        ImageView imageView = (ImageView) findViewById(R.id.likert_page_icon);
        imageView.setImageResource(R.drawable.battery4);
        // set color
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.logo_container);
        linearLayout.setBackgroundColor(0xAA11B0FF);
        // set footer
        TextView footer = (TextView) findViewById(R.id.footer_text);
        footer.setText(R.string.page22);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        List<NameValuePair> questions = new ArrayList<NameValuePair>();
        questions.add(new BasicNameValuePair("9001", getString(R.string.riasec_1)));
        questions.add(new BasicNameValuePair("9002", getString(R.string.riasec_2)));
        questions.add(new BasicNameValuePair("9003", getString(R.string.riasec_3)));
        questions.add(new BasicNameValuePair("9004", getString(R.string.riasec_4)));
        questions.add(new BasicNameValuePair("9005", getString(R.string.riasec_5)));
        questions.add(new BasicNameValuePair("9006", getString(R.string.riasec_6)));
        questions.add(new BasicNameValuePair("9007", getString(R.string.riasec_7)));
        questions.add(new BasicNameValuePair("9008", getString(R.string.riasec_8)));
        questions.add(new BasicNameValuePair("9009", getString(R.string.riasec_9)));
        questions.add(new BasicNameValuePair("9010", getString(R.string.riasec_10)));
        questions.add(new BasicNameValuePair("9011", getString(R.string.riasec_11)));
        questions.add(new BasicNameValuePair("9012", getString(R.string.riasec_12)));
        questions.add(new BasicNameValuePair("9013", getString(R.string.riasec_13)));
        questions.add(new BasicNameValuePair("9014", getString(R.string.riasec_14)));
        questions.add(new BasicNameValuePair("9015", getString(R.string.riasec_15)));
        questions.add(new BasicNameValuePair("9016", getString(R.string.riasec_16)));
        questions.add(new BasicNameValuePair("9017", getString(R.string.riasec_17)));
        questions.add(new BasicNameValuePair("9018", getString(R.string.riasec_18)));
        questions.add(new BasicNameValuePair("9019", getString(R.string.riasec_19)));
        questions.add(new BasicNameValuePair("9020", getString(R.string.riasec_20)));
        questions.add(new BasicNameValuePair("9021", getString(R.string.riasec_21)));
        questions.add(new BasicNameValuePair("9022", getString(R.string.riasec_22)));
        questions.add(new BasicNameValuePair("9023", getString(R.string.riasec_23)));
        questions.add(new BasicNameValuePair("9024", getString(R.string.riasec_24)));
        questions.add(new BasicNameValuePair("9025", getString(R.string.riasec_25)));
        questions.add(new BasicNameValuePair("9026", getString(R.string.riasec_26)));
        questions.add(new BasicNameValuePair("9027", getString(R.string.riasec_27)));
        questions.add(new BasicNameValuePair("9028", getString(R.string.riasec_28)));
        questions.add(new BasicNameValuePair("9029", getString(R.string.riasec_29)));
        questions.add(new BasicNameValuePair("9030", getString(R.string.riasec_30)));
        questions.add(new BasicNameValuePair("9031", getString(R.string.riasec_31)));
        questions.add(new BasicNameValuePair("9032", getString(R.string.riasec_32)));
        questions.add(new BasicNameValuePair("9033", getString(R.string.riasec_33)));
        questions.add(new BasicNameValuePair("9034", getString(R.string.riasec_34)));
        questions.add(new BasicNameValuePair("9035", getString(R.string.riasec_35)));
        questions.add(new BasicNameValuePair("9036", getString(R.string.riasec_36)));
        questions.add(new BasicNameValuePair("9037", getString(R.string.riasec_37)));
        questions.add(new BasicNameValuePair("9038", getString(R.string.riasec_38)));
        questions.add(new BasicNameValuePair("9039", getString(R.string.riasec_39)));
        questions.add(new BasicNameValuePair("9040", getString(R.string.riasec_40)));
        questions.add(new BasicNameValuePair("9041", getString(R.string.riasec_41)));
        questions.add(new BasicNameValuePair("9042", getString(R.string.riasec_42)));
        questions.add(new BasicNameValuePair("9043", getString(R.string.riasec_43)));
        questions.add(new BasicNameValuePair("9044", getString(R.string.riasec_44)));
        questions.add(new BasicNameValuePair("9045", getString(R.string.riasec_45)));
        questions.add(new BasicNameValuePair("9046", getString(R.string.riasec_46)));
        questions.add(new BasicNameValuePair("9047", getString(R.string.riasec_47)));
        questions.add(new BasicNameValuePair("9048", getString(R.string.riasec_48)));

        int color1 = 0x00FFFFFF;
        int color2 = 0xBBFFFFFF;
        boolean isColor1 = true;

        fragmentList = new ArrayList<Likert5Fragment>();

        // display random order
        Collections.shuffle(questions);

        for (NameValuePair question : questions) {
            Likert5Fragment fragment = new Likert5Fragment();
            fragmentList.add(fragment);
            Bundle args = new Bundle();
            args.putString("question", (String) question.getValue());
            args.putString("id", question.getName());
            args.putInt("mode", 2);
            if (isColor1) {
                args.putInt("color", color1);
            } else {
                args.putInt("color", color2);
            }
            isColor1 = !isColor1;

            args.putString("button1", getString(R.string.likert_enjoy_1));
            args.putString("button2", getString(R.string.likert_enjoy_2));
            args.putString("button3", getString(R.string.likert_enjoy_3));
            args.putString("button4", getString(R.string.likert_enjoy_4));
            args.putString("button5", getString(R.string.likert_enjoy_5));

            fragment.setArguments(args);
            fragmentTransaction.add(R.id.container, fragment);
        }
        fragmentTransaction.commit();

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void nextPage(View view) {
        // check if all questions are answered
        boolean allAnswered = true;
        for (Likert5Fragment fragment : fragmentList) {
            allAnswered = allAnswered && fragment.isAnswered();
        }
        if (allAnswered) { // go to next page
            computeMyRating();
            new Thread(new SaveAnswersTask()).start();
            new Thread(new SaveRIASECTask()).start();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String state = prefs.getString("demographics_state", "");
            Intent intent;
            if (state.equals("finished")) {
                intent = new Intent(this, InterestsResultsActivity.class);
            }
            else {
                intent = new Intent(this, DemographicsActivity.class);
                intent.putExtra("nextActivity", "interests");
            }
            intent.putExtra("userId", userId);
            startActivity(intent);

        } else { // display error message
            this.findViewById(R.id.error).setVisibility(View.VISIBLE);
        }

    }

    class SaveAnswersTask implements Runnable {
        @Override
        public void run() {
            List<NameValuePair> answers = new ArrayList<NameValuePair>(fragmentList.size());
            for (Likert5Fragment fragment : fragmentList) {
                String questionId = fragment.getQuestionId();
                int answer = fragment.getAnswer();
                answers.add(new BasicNameValuePair(questionId, Integer.toString(answer)));
            }
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveAnswers(userId, answers);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("riasec_state", "finished");
            editor.commit();
        }
    }

    private void computeMyRating() {
        int r = 0, i = 0, a = 0, s = 0, e = 0, c = 0;

        for (Likert5Fragment fragment : fragmentList) {
            String questionId = fragment.getQuestionId();
            int answer = fragment.getAnswer();

            if(questionId.equals("9001")){ r+=answer; }
            if(questionId.equals("9002")){ r+=answer; }
            if(questionId.equals("9003")){ r+=answer; }
            if(questionId.equals("9004")){ r+=answer; }
            if(questionId.equals("9005")){ r+=answer; }
            if(questionId.equals("9006")){ r+=answer; }
            if(questionId.equals("9007")){ r+=answer; }
            if(questionId.equals("9008")){ r+=answer; }
            if(questionId.equals("9009")){ i+=answer; }
            if(questionId.equals("9010")){ i+=answer; }
            if(questionId.equals("9011")){ i+=answer; }
            if(questionId.equals("9012")){ i+=answer; }
            if(questionId.equals("9013")){ i+=answer; }
            if(questionId.equals("9014")){ i+=answer; }
            if(questionId.equals("9015")){ i+=answer; }
            if(questionId.equals("9016")){ i+=answer; }
            if(questionId.equals("9017")){ a+=answer; }
            if(questionId.equals("9018")){ a+=answer; }
            if(questionId.equals("9019")){ a+=answer; }
            if(questionId.equals("9020")){ a+=answer; }
            if(questionId.equals("9021")){ a+=answer; }
            if(questionId.equals("9022")){ a+=answer; }
            if(questionId.equals("9023")){ a+=answer; }
            if(questionId.equals("9024")){ a+=answer; }
            if(questionId.equals("9025")){ s+=answer; }
            if(questionId.equals("9026")){ s+=answer; }
            if(questionId.equals("9027")){ s+=answer; }
            if(questionId.equals("9028")){ s+=answer; }
            if(questionId.equals("9029")){ s+=answer; }
            if(questionId.equals("9030")){ s+=answer; }
            if(questionId.equals("9031")){ s+=answer; }
            if(questionId.equals("9032")){ s+=answer; }
            if(questionId.equals("9033")){ e+=answer; }
            if(questionId.equals("9034")){ e+=answer; }
            if(questionId.equals("9035")){ e+=answer; }
            if(questionId.equals("9036")){ e+=answer; }
            if(questionId.equals("9037")){ e+=answer; }
            if(questionId.equals("9038")){ e+=answer; }
            if(questionId.equals("9039")){ e+=answer; }
            if(questionId.equals("9040")){ e+=answer; }
            if(questionId.equals("9041")){ c+=answer; }
            if(questionId.equals("9042")){ c+=answer; }
            if(questionId.equals("9043")){ c+=answer; }
            if(questionId.equals("9044")){ c+=answer; }
            if(questionId.equals("9045")){ c+=answer; }
            if(questionId.equals("9046")){ c+=answer; }
            if(questionId.equals("9047")){ c+=answer; }
            if(questionId.equals("9048")){ c+=answer; }

        }

        myRating = new ArrayList<Double>();
        myRating.add(r/8.0);
        myRating.add(i/8.0);
        myRating.add(a/8.0);
        myRating.add(s/8.0);
        myRating.add(e/8.0);
        myRating.add(c/8.0);
    }

    private class SaveRIASECTask implements Runnable {
        @Override
        public void run() {
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveInterests(userId, myRating.get(0), myRating.get(1), myRating.get(2), myRating.get(3), myRating.get(4), myRating.get(5));
        }
    }

}
