package app.autoidlabs.ch.personalitytest;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import app.autoidlabs.ch.personalitytest.helper.HttpRequest;
import app.autoidlabs.ch.personalitytest.helper.Likert6Fragment;

public class WHO5QuestionsActivity extends Activity {

    public String userId;
    List<Likert6Fragment> fragmentList = null;

    ArrayList<String> myRating = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the userId from the intent
        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");

        setContentView(R.layout.activity_likert_page);

        getActionBar().hide();

        // set title
        TextView titleView = (TextView) findViewById(R.id.title_activity_likert_page_text);
        titleView.setText(R.string.title_activity_who5);
        // set icon
        ImageView imageView = (ImageView) findViewById(R.id.likert_page_icon);
        imageView.setImageResource(R.drawable.battery5);
        // set color
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.logo_container);
        linearLayout.setBackgroundColor(0xAAB45FFF);
        // set footer
        TextView footer = (TextView) findViewById(R.id.footer_text);
        footer.setText(R.string.page11);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        List<NameValuePair> questions = new ArrayList<NameValuePair>();
        questions.add(new BasicNameValuePair("10001", getString(R.string.who5_1)));
        questions.add(new BasicNameValuePair("10002", getString(R.string.who5_2)));
        questions.add(new BasicNameValuePair("10003", getString(R.string.who5_3)));
        questions.add(new BasicNameValuePair("10004", getString(R.string.who5_4)));
        questions.add(new BasicNameValuePair("10005", getString(R.string.who5_5)));

        int color1 = 0x00FFFFFF;
        int color2 = 0xBBFFFFFF;
        boolean isColor1 = true;

        fragmentList = new ArrayList<Likert6Fragment>();

        // display random order
        //Collections.shuffle(questions);

        for (NameValuePair question : questions) {
            Likert6Fragment fragment = new Likert6Fragment();
            fragmentList.add(fragment);
            Bundle args = new Bundle();
            args.putString("question", (String) question.getValue());
            args.putString("id", question.getName());
            args.putInt("mode", 2);
            if (isColor1) {
                args.putInt("color", color1);
            } else {
                args.putInt("color", color2);
            }
            isColor1 = !isColor1;

            args.putString("button1", getString(R.string.likert_time_1));
            args.putString("button2", getString(R.string.likert_time_2));
            args.putString("button3", getString(R.string.likert_time_3));
            args.putString("button4", getString(R.string.likert_time_4));
            args.putString("button5", getString(R.string.likert_time_5));
            args.putString("button6", getString(R.string.likert_time_6));

            fragment.setArguments(args);
            fragmentTransaction.add(R.id.container, fragment);
        }
        fragmentTransaction.commit();

    }

    public void nextPage(View view) {
        // check if all questions are answered
        boolean allAnswered = true;
        for (Likert6Fragment fragment : fragmentList) {
            allAnswered = allAnswered && fragment.isAnswered();
        }
        if (allAnswered) { // go to next page
            new Thread(new SaveAnswersAndScoreTask()).start();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String state = prefs.getString("demographics_state", "");
            Intent intent;
            if (state.equals("finished")) {
                intent = new Intent(this, WHO5ResultsActivity.class);
            }
            else {
                intent = new Intent(this, DemographicsActivity.class);
                intent.putExtra("nextActivity", "who5");
            }
            intent.putExtra("userId", userId);
            startActivity(intent);

        } else { // display error message
            this.findViewById(R.id.error).setVisibility(View.VISIBLE);
        }

    }

    class SaveAnswersAndScoreTask implements Runnable {
        @Override
        public void run() {
            int score = 0;

            List<NameValuePair> answers = new ArrayList<NameValuePair>(fragmentList.size());
            for (Likert6Fragment fragment : fragmentList) {
                String questionId = fragment.getQuestionId();
                int answer = fragment.getAnswer();
                answers.add(new BasicNameValuePair(questionId, Integer.toString(answer)));
                score += answer;
            }

            // store score into the answer table
            answers.add(new BasicNameValuePair("10006", Integer.toString(4*score)));

            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveAnswers(userId, answers);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("who5_state", "finished");
            editor.commit();
        }
    }


}
