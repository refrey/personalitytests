package app.autoidlabs.ch.personalitytest;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import app.autoidlabs.ch.personalitytest.helper.HttpRequest;
import app.autoidlabs.ch.personalitytest.helper.Likert5Fragment;

public class FinanceRiskQuestionsActivity extends Activity {

    public String userId;
    List<Likert5Fragment> fragmentList = null;

    ArrayList<String> myRating = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the userId from the intent
        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");

        setContentView(R.layout.activity_likert_page);

        getActionBar().hide();

        // set title
        TextView titleView = (TextView) findViewById(R.id.title_activity_likert_page_text);
        titleView.setText(R.string.title_activity_finance_risk);
        // set icon
        ImageView imageView = (ImageView) findViewById(R.id.likert_page_icon);
        imageView.setImageResource(R.drawable.battery1);
        // set color
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.logo_container);
        linearLayout.setBackgroundColor(0xAAFF0000);
        // set footer
        TextView footer = (TextView) findViewById(R.id.footer_text);
        footer.setText(R.string.page22);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        List<NameValuePair> questions = new ArrayList<NameValuePair>();

        // questions about financial risks
        // source: A Domain-specific Risk-attitude Scale: Measuring Risk Perceptions and Risk Behaviors, Weber 2002
        questions.add(new BasicNameValuePair("528", getString(R.string.finance_1)));
        questions.add(new BasicNameValuePair("529", getString(R.string.finance_2)));
        questions.add(new BasicNameValuePair("530", getString(R.string.finance_3)));
        questions.add(new BasicNameValuePair("531", getString(R.string.finance_4)));
        questions.add(new BasicNameValuePair("532", getString(R.string.finance_5)));
        questions.add(new BasicNameValuePair("533", getString(R.string.finance_6)));
        questions.add(new BasicNameValuePair("534", getString(R.string.finance_7)));
        questions.add(new BasicNameValuePair("535", getString(R.string.finance_8)));
        questions.add(new BasicNameValuePair("536", getString(R.string.finance_9)));
        questions.add(new BasicNameValuePair("537", getString(R.string.finance_10)));


        int color1 = 0x00FFFFFF;
        int color2 = 0xBBFFFFFF;
        boolean isColor1 = true;

        fragmentList = new ArrayList<Likert5Fragment>();

        // display random order
        //Collections.shuffle(questions);

        for (NameValuePair question : questions) {
            Likert5Fragment fragment = new Likert5Fragment();
            fragmentList.add(fragment);
            Bundle args = new Bundle();
            args.putString("question", (String) question.getValue());
            args.putString("id", question.getName());
            if (isColor1) {
                args.putInt("color", color1);
            } else {
                args.putInt("color", color2);
            }
            isColor1 = !isColor1;

            args.putString("button1", getString(R.string.likert_likely_1));
            args.putString("button2", getString(R.string.likert_likely_2));
            args.putString("button3", getString(R.string.likert_likely_3));
            args.putString("button4", getString(R.string.likert_likely_4));
            args.putString("button5", getString(R.string.likert_likely_5));

            fragment.setArguments(args);
            fragmentTransaction.add(R.id.container, fragment);
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void nextPage(View view) {
        // check if all questions are answered
        boolean allAnswered = true;
        for (Likert5Fragment fragment : fragmentList) {
            allAnswered = allAnswered && fragment.isAnswered();
        }
        if (allAnswered) { // go to next page
            new Thread(new SaveAnswersTask()).start();

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String state = prefs.getString("demographics_state", "");
            Intent intent;
            if (state.equals("finished")) {
                intent = new Intent(this, DarkTriadResultsActivity.class);
            }
            else {
                intent = new Intent(this, DemographicsActivity.class);
                intent.putExtra("nextActivity", "dark_triad");  // The risk finance questions are part of the Dark Triad questionnaire.
            }
            intent.putExtra("userId", userId);
            startActivity(intent);

        } else { // display error message
            this.findViewById(R.id.error).setVisibility(View.VISIBLE);
        }

    }

    class SaveAnswersTask implements Runnable {
        @Override
        public void run() {
            List<NameValuePair> answers = new ArrayList<NameValuePair>(fragmentList.size());
            for (Likert5Fragment fragment : fragmentList) {
                String questionId = fragment.getQuestionId();
                int answer = fragment.getAnswer();
                answers.add(new BasicNameValuePair(questionId, Integer.toString(answer)));
            }
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveAnswers(userId, answers);

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("finance_risk_state", "finished");
            editor.commit();
        }
    }

}
