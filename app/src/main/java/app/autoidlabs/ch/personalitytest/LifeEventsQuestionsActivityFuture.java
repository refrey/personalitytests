package app.autoidlabs.ch.personalitytest;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import app.autoidlabs.ch.personalitytest.helper.HttpRequest;
import app.autoidlabs.ch.personalitytest.helper.MySeekBar;

public class LifeEventsQuestionsActivityFuture extends Activity {

    public String userId;

    MySeekBar mySeekBar1, mySeekBar2, mySeekBar3, mySeekBar4, mySeekBar5, mySeekBar6, mySeekBar7, mySeekBar8, mySeekBar9, mySeekBar10;
    MySeekBar mySeekBar11, mySeekBar12, mySeekBar13, mySeekBar14, mySeekBar15, mySeekBar16, mySeekBar17, mySeekBar18, mySeekBar19, mySeekBar20;
    MySeekBar mySeekBar21, mySeekBar22, mySeekBar23, mySeekBar24, mySeekBar25, mySeekBar26, mySeekBar27, mySeekBar28, mySeekBar29, mySeekBar30, mySeekBar31, mySeekBar32;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // get the userId from the intent
        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");

        setContentView(R.layout.activity_life_events);

        getActionBar().hide();

        // set title
        TextView titleView = (TextView) findViewById(R.id.title_activity_life_events_text);
        titleView.setText(R.string.title_activity_life_events_future);
        // set footer
        TextView footer = (TextView) findViewById(R.id.footer_text);
        footer.setText(R.string.page22);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        String at = getString(R.string.life_event_extra_answer_future);
        String qt = getString(R.string.life_event_extra_question_future);

        mySeekBar1 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_1), (SeekBar) findViewById(R.id.seekbar_life_event_1), (TextView) findViewById(R.id.info_life_event_1), qt, at);
        mySeekBar2 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_2), (SeekBar) findViewById(R.id.seekbar_life_event_2), (TextView) findViewById(R.id.info_life_event_2), qt, at);
        mySeekBar3 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_3), (SeekBar) findViewById(R.id.seekbar_life_event_3), (TextView) findViewById(R.id.info_life_event_3), qt, at);
        mySeekBar4 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_4), (SeekBar) findViewById(R.id.seekbar_life_event_4), (TextView) findViewById(R.id.info_life_event_4), qt, at);
        mySeekBar5 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_5), (SeekBar) findViewById(R.id.seekbar_life_event_5), (TextView) findViewById(R.id.info_life_event_5), qt, at);
        mySeekBar6 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_6), (SeekBar) findViewById(R.id.seekbar_life_event_6), (TextView) findViewById(R.id.info_life_event_6), qt, at);
        mySeekBar7 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_7), (SeekBar) findViewById(R.id.seekbar_life_event_7), (TextView) findViewById(R.id.info_life_event_7), qt, at);
        mySeekBar8 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_8), (SeekBar) findViewById(R.id.seekbar_life_event_8), (TextView) findViewById(R.id.info_life_event_8), qt, at);
        mySeekBar9 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_9), (SeekBar) findViewById(R.id.seekbar_life_event_9), (TextView) findViewById(R.id.info_life_event_9), qt, at);
        mySeekBar10 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_10), (SeekBar) findViewById(R.id.seekbar_life_event_10), (TextView) findViewById(R.id.info_life_event_10), qt, at);
        mySeekBar11 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_11), (SeekBar) findViewById(R.id.seekbar_life_event_11), (TextView) findViewById(R.id.info_life_event_11), qt, at);
        mySeekBar12 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_12), (SeekBar) findViewById(R.id.seekbar_life_event_12), (TextView) findViewById(R.id.info_life_event_12), qt, at);
        mySeekBar13 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_13), (SeekBar) findViewById(R.id.seekbar_life_event_13), (TextView) findViewById(R.id.info_life_event_13), qt, at);
        mySeekBar14 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_14), (SeekBar) findViewById(R.id.seekbar_life_event_14), (TextView) findViewById(R.id.info_life_event_14), qt, at);
        mySeekBar15 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_15), (SeekBar) findViewById(R.id.seekbar_life_event_15), (TextView) findViewById(R.id.info_life_event_15), qt, at);
        mySeekBar16 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_16), (SeekBar) findViewById(R.id.seekbar_life_event_16), (TextView) findViewById(R.id.info_life_event_16), qt, at);
        mySeekBar17 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_17), (SeekBar) findViewById(R.id.seekbar_life_event_17), (TextView) findViewById(R.id.info_life_event_17), qt, at);
        mySeekBar18 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_18), (SeekBar) findViewById(R.id.seekbar_life_event_18), (TextView) findViewById(R.id.info_life_event_18), qt, at);
        mySeekBar19 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_19), (SeekBar) findViewById(R.id.seekbar_life_event_19), (TextView) findViewById(R.id.info_life_event_19), qt, at);
        mySeekBar20 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_20), (SeekBar) findViewById(R.id.seekbar_life_event_20), (TextView) findViewById(R.id.info_life_event_20), qt, at);
        mySeekBar21 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_21), (SeekBar) findViewById(R.id.seekbar_life_event_21), (TextView) findViewById(R.id.info_life_event_21), qt, at);
        mySeekBar22 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_22), (SeekBar) findViewById(R.id.seekbar_life_event_22), (TextView) findViewById(R.id.info_life_event_22), qt, at);
        mySeekBar23 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_23), (SeekBar) findViewById(R.id.seekbar_life_event_23), (TextView) findViewById(R.id.info_life_event_23), qt, at);
        mySeekBar24 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_24), (SeekBar) findViewById(R.id.seekbar_life_event_24), (TextView) findViewById(R.id.info_life_event_24), qt, at);
        mySeekBar25 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_25), (SeekBar) findViewById(R.id.seekbar_life_event_25), (TextView) findViewById(R.id.info_life_event_25), qt, at);
        mySeekBar26 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_26), (SeekBar) findViewById(R.id.seekbar_life_event_26), (TextView) findViewById(R.id.info_life_event_26), qt, at);
        mySeekBar27 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_27), (SeekBar) findViewById(R.id.seekbar_life_event_27), (TextView) findViewById(R.id.info_life_event_27), qt, at);
        mySeekBar28 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_28), (SeekBar) findViewById(R.id.seekbar_life_event_28), (TextView) findViewById(R.id.info_life_event_28), qt, at);
        mySeekBar29 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_29), (SeekBar) findViewById(R.id.seekbar_life_event_29), (TextView) findViewById(R.id.info_life_event_29), qt, at);
        mySeekBar30 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_30), (SeekBar) findViewById(R.id.seekbar_life_event_30), (TextView) findViewById(R.id.info_life_event_30), qt, at);
        mySeekBar31 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_31), (SeekBar) findViewById(R.id.seekbar_life_event_31), (TextView) findViewById(R.id.info_life_event_31), qt, at);
        mySeekBar32 = new MySeekBar((LinearLayout) findViewById(R.id.full_seekbar_life_event_32), (SeekBar) findViewById(R.id.seekbar_life_event_32), (TextView) findViewById(R.id.info_life_event_32), qt, at);
    }

    public void onCheckboxClicked(View view) {
        // Is the button now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.checkbox_life_event_1:
                mySeekBar1.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_2:
                mySeekBar2.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_3:
                mySeekBar3.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_4:
                mySeekBar4.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_5:
                mySeekBar5.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_6:
                mySeekBar6.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_7:
                mySeekBar7.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_8:
                mySeekBar8.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_9:
                mySeekBar9.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_10:
                mySeekBar10.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_11:
                mySeekBar11.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_12:
                mySeekBar12.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_13:
                mySeekBar13.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_14:
                mySeekBar14.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_15:
                mySeekBar15.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_16:
                mySeekBar16.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_17:
                mySeekBar17.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_18:
                mySeekBar18.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_19:
                mySeekBar19.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_20:
                mySeekBar20.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_21:
                mySeekBar21.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_22:
                mySeekBar22.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_23:
                mySeekBar23.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_24:
                mySeekBar24.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_25:
                mySeekBar25.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_26:
                mySeekBar26.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_27:
                mySeekBar27.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_28:
                mySeekBar28.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_29:
                mySeekBar29.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_30:
                mySeekBar30.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_31:
                mySeekBar31.onCheckBoxClick(checked);
                break;
            case R.id.checkbox_life_event_32:
                mySeekBar32.onCheckBoxClick(checked);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    public void nextPage(View view) {
        // check if all questions are answered
        if (mySeekBar1.isReady() &&
                mySeekBar2.isReady() &&
                mySeekBar3.isReady() &&
                mySeekBar4.isReady() &&
                mySeekBar5.isReady() &&
                mySeekBar6.isReady() &&
                mySeekBar7.isReady() &&
                mySeekBar8.isReady() &&
                mySeekBar9.isReady() &&
                mySeekBar10.isReady() &&
                mySeekBar11.isReady() &&
                mySeekBar12.isReady() &&
                mySeekBar13.isReady() &&
                mySeekBar14.isReady() &&
                mySeekBar15.isReady() &&
                mySeekBar16.isReady() &&
                mySeekBar17.isReady() &&
                mySeekBar18.isReady() &&
                mySeekBar19.isReady() &&
                mySeekBar20.isReady() &&
                mySeekBar21.isReady() &&
                mySeekBar22.isReady() &&
                mySeekBar23.isReady() &&
                mySeekBar24.isReady() &&
                mySeekBar25.isReady() &&
                mySeekBar26.isReady() &&
                mySeekBar27.isReady() &&
                mySeekBar28.isReady() &&
                mySeekBar29.isReady() &&
                mySeekBar30.isReady() &&
                mySeekBar31.isReady() &&
                mySeekBar32.isReady() &&
                (!mySeekBar20.isChecked() || (mySeekBar20.isChecked() &&  answerIsNotEmpty(R.id.text_life_event_20b))) &&
                (!mySeekBar23.isChecked() || (mySeekBar23.isChecked() &&  answerIsNotEmpty(R.id.text_life_event_23b))) &&
                (!mySeekBar25.isChecked() || (mySeekBar25.isChecked() &&  answerIsNotEmpty(R.id.text_life_event_25b))) &&
                (!mySeekBar26.isChecked() || (mySeekBar26.isChecked() &&  answerIsNotEmpty(R.id.text_life_event_26b))) &&
                (!mySeekBar27.isChecked() || (mySeekBar27.isChecked() &&  answerIsNotEmpty(R.id.text_life_event_27b))) &&
                (!mySeekBar28.isChecked() || (mySeekBar28.isChecked() &&  answerIsNotEmpty(R.id.text_life_event_28b))) &&
                (!mySeekBar29.isChecked() || (mySeekBar29.isChecked() &&  answerIsNotEmpty(R.id.text_life_event_29b))) &&
                (!mySeekBar30.isChecked() || (mySeekBar30.isChecked() &&  answerIsNotEmpty(R.id.text_life_event_30b)))
                ) { // go to next page
            new Thread(new SaveAnswersTask()).start();
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String state = prefs.getString("demographics_state", "");
            Intent intent;
            if (state.equals("finished")) {
                intent = new Intent(this, LifeEventsResultsActivity.class);
            }
            else {
                intent = new Intent(this, DemographicsActivity.class);
                intent.putExtra("nextActivity", "life_events");
            }
            intent.putExtra("userId", userId);
            startActivity(intent);
        } else { // display error message
            this.findViewById(R.id.error).setVisibility(View.VISIBLE);

            if (mySeekBar1.isReady()) { mySeekBar1.setBlack(); } else { mySeekBar1.setRed(); }
            if (mySeekBar2.isReady()) { mySeekBar2.setBlack(); } else { mySeekBar2.setRed(); }
            if (mySeekBar3.isReady()) { mySeekBar3.setBlack(); } else { mySeekBar3.setRed(); }
            if (mySeekBar4.isReady()) { mySeekBar4.setBlack(); } else { mySeekBar4.setRed(); }
            if (mySeekBar5.isReady()) { mySeekBar5.setBlack(); } else { mySeekBar5.setRed(); }
            if (mySeekBar6.isReady()) { mySeekBar6.setBlack(); } else { mySeekBar6.setRed(); }
            if (mySeekBar7.isReady()) { mySeekBar7.setBlack(); } else { mySeekBar7.setRed(); }
            if (mySeekBar8.isReady()) { mySeekBar8.setBlack(); } else { mySeekBar8.setRed(); }
            if (mySeekBar9.isReady()) { mySeekBar9.setBlack(); } else { mySeekBar9.setRed(); }
            if (mySeekBar10.isReady()) { mySeekBar10.setBlack(); } else { mySeekBar10.setRed(); }
            if (mySeekBar11.isReady()) { mySeekBar11.setBlack(); } else { mySeekBar11.setRed(); }
            if (mySeekBar12.isReady()) { mySeekBar12.setBlack(); } else { mySeekBar12.setRed(); }
            if (mySeekBar13.isReady()) { mySeekBar13.setBlack(); } else { mySeekBar13.setRed(); }
            if (mySeekBar14.isReady()) { mySeekBar14.setBlack(); } else { mySeekBar14.setRed(); }
            if (mySeekBar15.isReady()) { mySeekBar15.setBlack(); } else { mySeekBar15.setRed(); }
            if (mySeekBar16.isReady()) { mySeekBar16.setBlack(); } else { mySeekBar16.setRed(); }
            if (mySeekBar17.isReady()) { mySeekBar17.setBlack(); } else { mySeekBar17.setRed(); }
            if (mySeekBar18.isReady()) { mySeekBar18.setBlack(); } else { mySeekBar18.setRed(); }
            if (mySeekBar19.isReady()) { mySeekBar19.setBlack(); } else { mySeekBar19.setRed(); }
            if (mySeekBar20.isReady()) { mySeekBar20.setBlack(); } else { mySeekBar20.setRed(); }
            if (mySeekBar21.isReady()) { mySeekBar21.setBlack(); } else { mySeekBar21.setRed(); }
            if (mySeekBar22.isReady()) { mySeekBar22.setBlack(); } else { mySeekBar22.setRed(); }
            if (mySeekBar23.isReady()) { mySeekBar23.setBlack(); } else { mySeekBar23.setRed(); }
            if (mySeekBar24.isReady()) { mySeekBar24.setBlack(); } else { mySeekBar24.setRed(); }
            if (mySeekBar25.isReady()) { mySeekBar25.setBlack(); } else { mySeekBar25.setRed(); }
            if (mySeekBar26.isReady()) { mySeekBar26.setBlack(); } else { mySeekBar26.setRed(); }
            if (mySeekBar27.isReady()) { mySeekBar27.setBlack(); } else { mySeekBar27.setRed(); }
            if (mySeekBar28.isReady()) { mySeekBar28.setBlack(); } else { mySeekBar28.setRed(); }
            if (mySeekBar29.isReady()) { mySeekBar29.setBlack(); } else { mySeekBar29.setRed(); }
            if (mySeekBar30.isReady()) { mySeekBar30.setBlack(); } else { mySeekBar30.setRed(); }
            if (mySeekBar31.isReady()) { mySeekBar31.setBlack(); } else { mySeekBar31.setRed(); }
            if (mySeekBar32.isReady()) { mySeekBar32.setBlack(); } else { mySeekBar32.setRed(); }

            if (mySeekBar20.isChecked() && !answerIsNotEmpty(R.id.text_life_event_20b)) { ((TextView) findViewById(R.id.detail_life_event_20)).setTextColor(Color.RED); }
            else {((TextView) findViewById(R.id.detail_life_event_20)).setTextColor(Color.BLACK); }
            if (mySeekBar23.isChecked() && !answerIsNotEmpty(R.id.text_life_event_23b)) { ((TextView) findViewById(R.id.detail_life_event_23)).setTextColor(Color.RED); }
            else {((TextView) findViewById(R.id.detail_life_event_23)).setTextColor(Color.BLACK); }
            if (mySeekBar25.isChecked() && !answerIsNotEmpty(R.id.text_life_event_25b)) { ((TextView) findViewById(R.id.detail_life_event_25)).setTextColor(Color.RED); }
            else {((TextView) findViewById(R.id.detail_life_event_25)).setTextColor(Color.BLACK); }
            if (mySeekBar26.isChecked() && !answerIsNotEmpty(R.id.text_life_event_26b)) { ((TextView) findViewById(R.id.detail_life_event_26)).setTextColor(Color.RED); }
            else {((TextView) findViewById(R.id.detail_life_event_26)).setTextColor(Color.BLACK); }
            if (mySeekBar27.isChecked() && !answerIsNotEmpty(R.id.text_life_event_27b)) { ((TextView) findViewById(R.id.detail_life_event_27)).setTextColor(Color.RED); }
            else {((TextView) findViewById(R.id.detail_life_event_27)).setTextColor(Color.BLACK); }
            if (mySeekBar28.isChecked() && !answerIsNotEmpty(R.id.text_life_event_28b)) { ((TextView) findViewById(R.id.detail_life_event_28)).setTextColor(Color.RED); }
            else {((TextView) findViewById(R.id.detail_life_event_28)).setTextColor(Color.BLACK); }
            if (mySeekBar29.isChecked() && !answerIsNotEmpty(R.id.text_life_event_29b)) { ((TextView) findViewById(R.id.detail_life_event_29)).setTextColor(Color.RED); }
            else {((TextView) findViewById(R.id.detail_life_event_29)).setTextColor(Color.BLACK); }
            if (mySeekBar29.isChecked() && !answerIsNotEmpty(R.id.text_life_event_30b)) { ((TextView) findViewById(R.id.detail_life_event_30)).setTextColor(Color.RED); }
            else {((TextView) findViewById(R.id.detail_life_event_30)).setTextColor(Color.BLACK); }
        }
    }

    private boolean answerIsNotEmpty(int id) {
        return !((EditText) findViewById(id)).getText().toString().matches("");
    }

    class SaveAnswersTask implements Runnable {
        @Override
        public void run() {
            int we = 0;
            int f = 0;
            int hl = 0;
            int hw = 0;
            int te = 0;
            List<NameValuePair> answers = new ArrayList<NameValuePair>();

            if (mySeekBar1.isChecked()) { answers.add(new BasicNameValuePair("2001", mySeekBar1.getStringValue())); we++; }
            if (mySeekBar2.isChecked()) { answers.add(new BasicNameValuePair("2002", mySeekBar2.getStringValue())); we++; }
            if (mySeekBar3.isChecked()) { answers.add(new BasicNameValuePair("2003", mySeekBar3.getStringValue())); we++; }
            if (mySeekBar4.isChecked()) { answers.add(new BasicNameValuePair("2004", mySeekBar4.getStringValue())); we++; }
            if (mySeekBar5.isChecked()) { answers.add(new BasicNameValuePair("2005", mySeekBar5.getStringValue())); f++; }
            if (mySeekBar6.isChecked()) { answers.add(new BasicNameValuePair("2006", mySeekBar6.getStringValue())); f++; }
            if (mySeekBar7.isChecked()) { answers.add(new BasicNameValuePair("2007", mySeekBar7.getStringValue())); f++; }
            if (mySeekBar8.isChecked()) { answers.add(new BasicNameValuePair("2008", mySeekBar8.getStringValue())); f++; }
            if (mySeekBar9.isChecked()) { answers.add(new BasicNameValuePair("2009", mySeekBar9.getStringValue())); f++; }
            if (mySeekBar10.isChecked()) { answers.add(new BasicNameValuePair("2010", mySeekBar10.getStringValue())); f++; }
            if (mySeekBar11.isChecked()) { answers.add(new BasicNameValuePair("2011", mySeekBar11.getStringValue())); f++; }
            if (mySeekBar12.isChecked()) { answers.add(new BasicNameValuePair("2012", mySeekBar12.getStringValue())); f++; }
            if (mySeekBar13.isChecked()) { answers.add(new BasicNameValuePair("2013", mySeekBar13.getStringValue())); f++; }
            if (mySeekBar14.isChecked()) { answers.add(new BasicNameValuePair("2014", mySeekBar14.getStringValue())); f++; }
            if (mySeekBar15.isChecked()) { answers.add(new BasicNameValuePair("2015", mySeekBar15.getStringValue())); hl++; }
            if (mySeekBar16.isChecked()) { answers.add(new BasicNameValuePair("2016", mySeekBar16.getStringValue())); hl++; }
            if (mySeekBar17.isChecked()) { answers.add(new BasicNameValuePair("2017", mySeekBar17.getStringValue())); hl++; }
            if (mySeekBar18.isChecked()) { answers.add(new BasicNameValuePair("2018", mySeekBar18.getStringValue())); hl++; }
            if (mySeekBar19.isChecked()) { answers.add(new BasicNameValuePair("2019", mySeekBar19.getStringValue())); hl++; }
            if (mySeekBar20.isChecked()) { answers.add(new BasicNameValuePair("2020", mySeekBar20.getStringValue())); hl++;
                answers.add(new BasicNameValuePair("4020", ((EditText) findViewById(R.id.text_life_event_20b)).getText().toString())); }
            if (mySeekBar21.isChecked()) { answers.add(new BasicNameValuePair("2021", mySeekBar21.getStringValue())); hw++; }
            if (mySeekBar22.isChecked()) { answers.add(new BasicNameValuePair("2022", mySeekBar22.getStringValue())); hw++; }
            if (mySeekBar23.isChecked()) { answers.add(new BasicNameValuePair("2023", mySeekBar23.getStringValue())); hw++;
                answers.add(new BasicNameValuePair("4023", ((EditText) findViewById(R.id.text_life_event_23b)).getText().toString())); }
            if (mySeekBar24.isChecked()) { answers.add(new BasicNameValuePair("2024", mySeekBar24.getStringValue())); hw++; }
            if (mySeekBar25.isChecked()) { answers.add(new BasicNameValuePair("2025", mySeekBar25.getStringValue())); te++;
                answers.add(new BasicNameValuePair("4025", ((EditText) findViewById(R.id.text_life_event_25b)).getText().toString())); }
            if (mySeekBar26.isChecked()) { answers.add(new BasicNameValuePair("2026", mySeekBar26.getStringValue())); te++;
                answers.add(new BasicNameValuePair("4026", ((EditText) findViewById(R.id.text_life_event_26b)).getText().toString())); }
            if (mySeekBar27.isChecked()) { answers.add(new BasicNameValuePair("2027", mySeekBar27.getStringValue())); te++;
                answers.add(new BasicNameValuePair("4027", ((EditText) findViewById(R.id.text_life_event_27b)).getText().toString())); }
            if (mySeekBar28.isChecked()) { answers.add(new BasicNameValuePair("2028", mySeekBar28.getStringValue())); te++;
                answers.add(new BasicNameValuePair("4028", ((EditText) findViewById(R.id.text_life_event_28b)).getText().toString())); }
            if (mySeekBar29.isChecked()) { answers.add(new BasicNameValuePair("2029", mySeekBar29.getStringValue())); te++;
                answers.add(new BasicNameValuePair("4029", ((EditText) findViewById(R.id.text_life_event_29b)).getText().toString())); }
            if (mySeekBar30.isChecked()) { answers.add(new BasicNameValuePair("2030", mySeekBar30.getStringValue())); te++;
                answers.add(new BasicNameValuePair("4030", ((EditText) findViewById(R.id.text_life_event_30b)).getText().toString())); }
            if (mySeekBar31.isChecked()) { answers.add(new BasicNameValuePair("2031", mySeekBar31.getStringValue())); hl++; }
            if (mySeekBar32.isChecked()) { answers.add(new BasicNameValuePair("2032", mySeekBar32.getStringValue())); hw++; }

            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveAnswers(userId, answers);

            try {
                ArrayList<String> myRating = httpRequest.getMyLifeEventsRating(userId);
                httpRequest.saveLifeEvents(userId, Integer.parseInt(myRating.get(0))+we, Integer.parseInt(myRating.get(1))+f,
                        Integer.parseInt(myRating.get(2))+ hl, Integer.parseInt(myRating.get(3))+hw, Integer.parseInt(myRating.get(4))+te);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("life_events_future_state", "finished");
            editor.commit();
        }
    }

}

