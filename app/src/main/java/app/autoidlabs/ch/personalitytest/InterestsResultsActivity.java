package app.autoidlabs.ch.personalitytest;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;

import org.json.JSONException;

import java.util.ArrayList;

import app.autoidlabs.ch.personalitytest.helper.HttpRequest;
import app.autoidlabs.ch.personalitytest.helper.MyMarkerView;

public class InterestsResultsActivity extends FragmentActivity {

    private static RadarChart mChart;
    static Context context;
    private ProgressDialog dialog;
    private ByeFragment fragment;

    static ArrayList<String> avgRating = null;
    static ArrayList<String> myRating = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();

        setContentView(R.layout.activity_main);

        getActionBar().hide();

        dialog = ProgressDialog.show(this, "", context.getString(R.string.loading), true);

        fragment = new ByeFragment();
        getFragmentManager().beginTransaction().add(R.id.container, fragment).commit();

        new Thread(new getRatingsTask()).start();
        BroadcastReceiver resultReceiver = createBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(resultReceiver, new IntentFilter("interests_rating"));
    }

    private BroadcastReceiver createBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                avgRating = intent.getStringArrayListExtra("avgRating");
                myRating = intent.getStringArrayListExtra("myRating");
                dialog.hide();
                drawChart();

            }
        };
    }

    private class getRatingsTask implements Runnable {
        public void run() {
            HttpRequest httpRequest = new HttpRequest();
            ArrayList<String> avgRating = null;
            ArrayList<String> myRating = null;

            // get the userId from the intent
            String userId = getIntent().getStringExtra("userId");

            try {
                avgRating = httpRequest.getInterestsAvgRating();
                myRating = httpRequest.getMyInterestsRating(userId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // broadcast asynchronously
            Intent intent = new Intent("interests_rating");
            intent.putExtra("avgRating", avgRating);
            intent.putExtra("myRating", myRating);
            LocalBroadcastManager.getInstance(InterestsResultsActivity.this).sendBroadcast(intent);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    /**
     * A fragment to say thank you and goodbye.
     */
    static public class ByeFragment extends Fragment {

        View rootView;
        Activity activity;

        public ByeFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_interests_results, container, false);
            activity = this.getActivity();


            //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            //        WindowManager.LayoutParams.FLAG_FULLSCREEN);
            //setContentView(R.layout.activity_radarchart);


            return rootView;

        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.radar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.actionToggleValues: {
                for (DataSet<?> set : mChart.getData().getDataSets())
                    set.setDrawValues(!set.isDrawValuesEnabled());

                mChart.invalidate();
                break;
            }
            case R.id.actionToggleHighlight: {
                if (mChart.isHighlightEnabled())
                    mChart.setHighlightEnabled(false);
                else
                    mChart.setHighlightEnabled(true);
                mChart.invalidate();
                break;
            }
            case R.id.actionToggleRotate: {
                if (mChart.isRotationEnabled())
                    mChart.setRotationEnabled(false);
                else
                    mChart.setRotationEnabled(true);
                mChart.invalidate();
                break;
            }
            case R.id.actionToggleFilled: {

                ArrayList<RadarDataSet> sets = (ArrayList<RadarDataSet>) mChart.getData()
                        .getDataSets();

                for (RadarDataSet set : sets) {
                    if (set.isDrawFilledEnabled())
                        set.setDrawFilled(false);
                    else
                        set.setDrawFilled(true);
                }
                mChart.invalidate();
                break;
            }
            case R.id.actionSave: {
                if (mChart.saveToPath("title" + System.currentTimeMillis(), "")) {
                    Toast.makeText(getApplicationContext(), "Saving SUCCESSFUL!",
                            Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(getApplicationContext(), "Saving FAILED!", Toast.LENGTH_SHORT)
                            .show();
                break;
            }
            case R.id.actionToggleXLabels: {
                mChart.getXAxis().setEnabled(!mChart.getXAxis().isEnabled());
                mChart.invalidate();
                break;
            }
            case R.id.actionToggleYLabels: {

                mChart.getYAxis().setEnabled(!mChart.getYAxis().isEnabled());
                mChart.invalidate();
                break;
            }
            case R.id.actionToggleSpin: {
                mChart.spin(2000, mChart.getRotationAngle(), mChart.getRotationAngle() + 360);
                break;
            }
        }
        return true;
    }

    static public void setData() {

        //float mult = 150;
        //int cnt = 5;


        Log.i("=== in Bye", "show ratings");
        System.out.println(avgRating);
        System.out.println(myRating);

        ArrayList<Float> avgRating_t = new ArrayList<Float>();
        ArrayList<Float> avgMyRating_t = new ArrayList<Float>();
        for (int i = 0; i < 6; i++) {
            avgRating_t.add(Float.parseFloat((avgRating.get(i))));
            avgMyRating_t.add(Float.parseFloat((myRating.get(i))));
        }


        ArrayList<Entry> yVals1 = new ArrayList<>();
        ArrayList<Entry> yVals2 = new ArrayList<Entry>();
        for (int i = 0; i < 6; i++) {
            yVals1.add(new Entry(avgRating_t.get(i), i));
            yVals2.add(new Entry(avgMyRating_t.get(i), i));
        }


        /*
        yVals1.add(new Entry((float)3.14,0));
        yVals1.add(new Entry((float)4.24,1));
        yVals1.add(new Entry((float)3.34,2));
        yVals1.add(new Entry((float)4.44,3));
        yVals1.add(new Entry((float)3.54,4));

        yVals2.add(new Entry((float)2.14,0));
        yVals2.add(new Entry((float)3.24,1));
        yVals2.add(new Entry((float)2.34,2));
        yVals2.add(new Entry((float)3.44,3));
        yVals2.add(new Entry((float)2.54,4));
        */


        /*
        for (int i = 0; i < cnt; i++) {
            yVals1.add(new Entry((float)3.14,i));
            yVals1.add(new Entry((float)4.24,i));
            yVals1.add(new Entry((float)3.34,i));
            yVals1.add(new Entry((float)4.44,i));
            yVals1.add(new Entry((float)3.54,i));

            //yVals1.add(new Entry((float) (Math.random() * mult) + mult / 2, i));
        }

        for (int i = 0; i < cnt; i++) {
            //yVals2.add(new Entry((float) (Math.random() * mult) + mult / 2, i));
            yVals2.add(new Entry((float)2.14,i));
            yVals2.add(new Entry((float)3.24,i));
            yVals2.add(new Entry((float)2.34,i));
            yVals2.add(new Entry((float)3.44,i));
            yVals2.add(new Entry((float)2.54,i));

        }
        */
        ArrayList<String> xVals = new ArrayList<String>();

        xVals.add("  R  ");
        xVals.add("  I  ");
        xVals.add("  A  ");
        xVals.add("  S  ");
        xVals.add("  E  ");
        xVals.add("  C  ");

        /*
        for (int i = 0; i < cnt; i++) {
            //xVals.add(mParties[i]);
            //xVals.add(mParties[i % mParties.length]);

            //Log.i("==== in rader i ", String.valueOf(i));
            //Log.i("==== in rader value", String.valueOf(mParties[i]));

            //"Openness", "Conscientiousness", "Extraversion", "Agreeableness", "Neuroticism"
            xVals.add("Openness");
            xVals.add("Conscientiousness");
            xVals.add("Extraversion");
            xVals.add("Agreeableness");
            xVals.add("Neuroticism");

        }
        */


        RadarDataSet set1 = new RadarDataSet(yVals1, context.getString(R.string.average_rating));
        //set1.setColor(ColorTemplate.VORDIPLOM_COLORS[0]);
        set1.setColor(Color.rgb(0, 0, 0));
        set1.setDrawFilled(true);
        set1.setLineWidth(2f);

        RadarDataSet set2 = new RadarDataSet(yVals2, context.getString(R.string.your_rating));
        //set2.setColor(ColorTemplate.VORDIPLOM_COLORS[4]);
        set2.setColor(Color.rgb(17, 176, 255));
        set2.setDrawFilled(true);
        set2.setLineWidth(2f);

        ArrayList<RadarDataSet> sets = new ArrayList<RadarDataSet>();
        sets.add(set1);
        sets.add(set2);

        RadarData data = new RadarData(xVals, sets);
        //data.setValueTypeface(tf);
        data.setValueTextSize(8f);
        data.setDrawValues(false);

        mChart.setData(data);

        mChart.invalidate();
    }


    void drawChart () {
        mChart=(RadarChart)fragment.rootView.findViewById(R.id.chart_interests);

        //tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        mChart.setDescription("");

        mChart.setWebLineWidth(1.5f);
        mChart.setWebLineWidthInner(0.75f);
        mChart.setWebAlpha(100);

        // create a custom MarkerView (extend MarkerView) and specify the layout
        // to use for it
        MyMarkerView mv;
        mv=new

                MyMarkerView(context, R.layout.custom_marker_view);

        // set the marker to the chart
        mChart.setMarkerView(mv);

        setData();

        XAxis xAxis = mChart.getXAxis();
        //xAxis.setTypeface(tf);
        xAxis.setTextSize(9f);

        YAxis yAxis = mChart.getYAxis();
        //yAxis.setTypeface(tf);
        yAxis.setLabelCount(4);
        yAxis.setTextSize(9f);
        yAxis.setStartAtZero(false);
        yAxis.setAxisMaxValue((float)5);
        yAxis.setAxisMinValue((float)1);

        Legend l = mChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        //l.setTypeface(tf);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(5f);
    }

}
