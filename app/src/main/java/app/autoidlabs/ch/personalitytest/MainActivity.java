package app.autoidlabs.ch.personalitytest;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import app.autoidlabs.ch.personalitytest.helper.DisclosureDialogFragment;
import app.autoidlabs.ch.personalitytest.helper.HttpRequest;

public class MainActivity extends Activity implements DisclosureDialogFragment.NoticeDialogListener {

    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_welcome);

        getActionBar().hide();

        setupReadMoreFeature(R.id.introduction_big5, R.string.introduction_big5_short, R.string.introduction_big5_long);
        setupReadMoreFeature(R.id.introduction_life_events, R.string.introduction_life_events_short, R.string.introduction_life_events_long);
        setupReadMoreFeature(R.id.introduction_dark_triad, R.string.introduction_dark_triad_short, R.string.introduction_dark_triad_long);
        setupReadMoreFeature(R.id.introduction_interests, R.string.introduction_interests_short, R.string.introduction_interests_long);
        setupReadMoreFeature(R.id.introduction_who5, R.string.introduction_who5_short, R.string.introduction_who5_long);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String state1, state2;

        state1 = prefs.getString("big5_state", "");
        if (state1.equals("finished")) {
            Button button = (Button) findViewById(R.id.start_big5_button);
            button.setText(getResources().getText(R.string.result).toString());
        }
        state1 = prefs.getString("life_events_past_state", "");
        state2 = prefs.getString("life_events_future_state", "");
        if (state1.equals("finished")) {
            Button button = (Button) findViewById(R.id.start_life_events_button);
            if (state2.equals("finished")){
                button.setText(getResources().getText(R.string.result).toString());
            }
            else {
                button.setText(getResources().getText(R.string.complete).toString());
            }
        }
        state1 = prefs.getString("interests_state", "");
        state2 = prefs.getString("riasec_state", "");
        if (state1.equals("finished")) {
            Button button = (Button) findViewById(R.id.start_interests_button);
            if (state2.equals("finished")){
                button.setText(getResources().getText(R.string.result).toString());
            }
            else {
                button.setText(getResources().getText(R.string.complete).toString());
            }
        }
        state1 = prefs.getString("dark_triad_state", "");
        state2 = prefs.getString("finance_risk_state", "");
        if (state1.equals("finished")) {
            Button button = (Button) findViewById(R.id.start_dark_triad_button);
            if (state2.equals("finished")){
                button.setText(getResources().getText(R.string.result).toString());
            }
            else {
                button.setText(getResources().getText(R.string.complete).toString());
            }
        }
        state1 = prefs.getString("who5_state", "");
        if (state1.equals("finished")) {
            Button button = (Button) findViewById(R.id.start_who5_button);
            button.setText(getResources().getText(R.string.result).toString());
        }
        String disclosure = prefs.getString("disclosure", "");
        if (disclosure.equals("accepted")) {
            userId = prefs.getString("user_id", "");
        }
        else {
            showDisclosureDialog();
        }
    }

    public void showDisclosureDialog() {
        // Create an instance of the dialog fragment and show it
        DialogFragment dialogFragment = new DisclosureDialogFragment();
        dialogFragment.show(getFragmentManager(), "disclosure");
    }

    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the NoticeDialogFragment.NoticeDialogListener interface
    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        // User touched the dialog's positive button
        generateUserId();
        saveDisclosureAnswer("accepted");
        new Thread(new SaveUserTask()).start();
        new Thread(new SaveAppsTask()).start();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button
        saveDisclosureAnswer("refused");
        setContentView(R.layout.fragment_refused);
        getActionBar().hide();
    }

    private void saveDisclosureAnswer(String answer) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("disclosure", answer);
        editor.commit();
    }

    private void setupReadMoreFeature(int viewId, int shortTextId, int longTextId) {
        final TextView textView = (TextView) findViewById(viewId);
        final String shortText = getResources().getText(shortTextId).toString();
        final String longText = getResources().getText(longTextId).toString();

        textView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String readMoreString = getResources().getText(R.string.read_more).toString();
                String readLessString = getResources().getText(R.string.read_less).toString();
                if (textView.getText().toString().contains(readMoreString)) {
                    textView.setText(longText);
                } else if (textView.getText().toString().contains(readLessString)) {
                    textView.setText(shortText);
                }
            }
        });
    }

    private void generateUserId() {
        Random rand = new Random();
        userId =  String.valueOf(rand.nextInt(Integer.MAX_VALUE));
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("user_id", userId); // store generated userId permanently
        editor.commit();
    }

    // exit the app
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void startBig5(View view) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String state = prefs.getString("big5_state", "");
        Intent intent;
        if (state.equals("finished")) {
            intent = new Intent(this, Big5ResultsActivity.class);
        }
        else {
            intent = new Intent(this, Big5QuestionsActivity.class);
        }
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    public void startLifeEvents(View view) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String state1 = prefs.getString("life_events_past_state", "");
        String state2 = prefs.getString("life_events_future_state", "");
        Intent intent;
        if (state2.equals("finished")) {
            intent = new Intent(this, LifeEventsResultsActivity.class);
        }
        else if (state1.equals("finished")) {
            intent = new Intent(this, LifeEventsQuestionsActivityFuture.class);
        }
        else {
            intent = new Intent(this, LifeEventsQuestionsActivityPast.class);
        }
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    public void startInterests(View view) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String state1 = prefs.getString("interests_state", "");
        String state2 = prefs.getString("riasec_state", "");
        Intent intent;
        if (state2.equals("finished")) {
            intent = new Intent(this, InterestsResultsActivity.class);
        }
        else if (state1.equals("finished")) {
            intent = new Intent(this, InterestsRIASECQuestionsActivity.class);
        }
        else {
            intent = new Intent(this, InterestsQuestionsActivity.class);
        }
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    public void startDarkTriad(View view) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String state1 = prefs.getString("dark_triad_state", "");
        String state2 = prefs.getString("finance_risk_state", "");
        Intent intent;
        if (state2.equals("finished")) {
            intent = new Intent(this, DarkTriadResultsActivity.class);
        }
        else if (state1.equals("finished")) {
            intent = new Intent(this, FinanceRiskQuestionsActivity.class);
        }
        else {
            intent = new Intent(this, DarkTriadQuestionsActivity.class);
        }
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    public void startWHO5(View view) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String state = prefs.getString("who5_state", "");
        Intent intent;
        if (state.equals("finished")) {
            intent = new Intent(this, WHO5ResultsActivity.class);
        }
        else {
            intent = new Intent(this, WHO5QuestionsActivity.class);
        }
        intent.putExtra("userId", userId);
        startActivity(intent);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        String appId = getResources().getString(R.string.facebook_app_id);
        com.facebook.AppEventsLogger.activateApp(getApplicationContext(), appId);
    }

    private class SaveUserTask implements Runnable {
        @Override
        public void run() {
            HttpRequest httpRequest = new HttpRequest();
            httpRequest.saveUserWithAdId(userId, getApplicationContext());

        }
    }

    private class SaveAppsTask implements Runnable {
        @Override
        public void run() {
            HttpRequest httpRequest = new HttpRequest();
            final PackageManager pm = MainActivity.this.getPackageManager();

            List<ApplicationInfo> installedApplications = pm.getInstalledApplications(PackageManager.GET_META_DATA);

            ActivityManager actvityManager = (ActivityManager)
                    MainActivity.this.getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> procInfos = actvityManager.getRunningAppProcesses();
            List<String> runningApps = new ArrayList<String>();
            for(int i = 0; i < procInfos.size(); i++)
            {
                runningApps.add(procInfos.get(i).processName);
            }

            for (ApplicationInfo applicationInfo : installedApplications) {
                String packageName = applicationInfo.packageName;
                String appName = pm.getApplicationLabel(applicationInfo).toString();
                int isPreInstalled = 0;
                if ((android.content.pm.ApplicationInfo.FLAG_SYSTEM & applicationInfo.flags) != 0) {
                    isPreInstalled = 1;
                }
                int isRunning = 0;
                if (runningApps.contains(packageName)) {
                    isRunning = 1;
                }
                try {
                    int firstInstallTime = (int) (pm.getPackageInfo(packageName, 0).firstInstallTime / 1000);
                    int lastUpdateTime = (int) (pm.getPackageInfo(packageName, 0).lastUpdateTime / 1000);
                    String versionName = pm.getPackageInfo(packageName, 0).versionName;
                    httpRequest.saveApp(userId, appName, packageName, firstInstallTime, lastUpdateTime, versionName, isPreInstalled, isRunning);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}

